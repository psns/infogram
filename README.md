# Infogram

Le réseau social centré sur l'info.

## Prerequisites

| dependencies   | versions |
| -------------- | -------- |
| npm            | v7.11    |
| nodejs         | v15      |
| docker         | v20.10   |
| docker-compose | v1.29    |

### Install depedencies

```sh
npm install --include=dev
```

### Build prod

Build front (static files).

```sh
npm run build

```

### Docker Container

```bash
docker-compose up -d
```

Stop docker image.

```sh
docker-compose down
```

You can access to pgadmin using the following url : <http://localhost:8080>

You can access to postgres using the following url: <http://localhost:5454>

### PostgreSQL DB

Migrate all model to the database, create a sql file if there are changes.
Add migrate name in params

```bash
npm run prisma-build
```

Add default data to database (use only for development)

```sh
npm run prisma-seed
```

## Launch

Start Infogram.
You can access this server using the following url : <http://localhost:3000>

```sh
npm run serve
```

### Postman

You can test api server using this collection :

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/352179318b7dbbdf441e)
