const express = require('express')
const BadgeController = require('../controllers/badge')
const PostController = require('../controllers/post')
const ScoreController = require('../controllers/score')

const postRoutes = express.Router()

const {
	authenticateToken,
	authenticatePost,
	authenticateAdmin,
} = require('../middlewares/auth')

postRoutes.get('/badge', BadgeController.getAll)
postRoutes.get('/badge/:postId', BadgeController.getBadgePost)
postRoutes.delete(
	'/badge/:badgeId',
	authenticateToken,
	authenticateAdmin,
	BadgeController.delete
)
postRoutes.post(
	'/badge',
	authenticateToken,
	authenticateAdmin,
	BadgeController.create
)
postRoutes.put(
	'/badge/assign/:userId',
	authenticateToken,
	BadgeController.assign
)
postRoutes.put(
	'/badge/unassign/:userId',
	authenticateToken,
	BadgeController.unassign
)
postRoutes.put(
	'/badge/:badgeId',
	authenticateToken,
	authenticateAdmin,
	BadgeController.update
)
postRoutes.delete(
	'/badge/:badgeId',
	authenticateToken,
	authenticateAdmin,
	BadgeController.delete
)

postRoutes.get(
	'/score/:userId/:postId',
	authenticateToken,
	ScoreController.getUserScore
)
postRoutes.get('/score/:postId', ScoreController.getScore)

postRoutes.post('/score/:userId', authenticateToken, ScoreController.assign)

postRoutes.post('/popular', PostController.getPopularPost)
postRoutes.post('/tag/:tagName', PostController.getTagsPost)
postRoutes.get(
	'/follower/:userId',
	authenticateToken,
	PostController.getFollowerPost
)
postRoutes.get('/:userId', PostController.getPost)
postRoutes.get('/id/:postId', PostController.getPostById)
postRoutes.get('/response/:postId', PostController.getResponse)
postRoutes.get('/response/nb/:postId', PostController.getNbResponse)
postRoutes.post('/:userId', authenticateToken, PostController.create)
postRoutes.delete(
	'/:postId',
	authenticateToken,
	authenticatePost,
	PostController.delete
)

module.exports = postRoutes
