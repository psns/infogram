const express = require('express')
const FilerController = require('../controllers/filer')
const { authenticateToken, authenticateFiler } = require('../middlewares/auth')

const filerRoutes = express.Router()

filerRoutes.post(
	'/upload',
	authenticateToken,
	authenticateFiler,
	FilerController.upload
)
filerRoutes.get('/download/:file', authenticateToken, FilerController.download)

module.exports = filerRoutes
