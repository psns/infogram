const express = require('express')
const TagController = require('../controllers/tag')

const tagRoutes = express.Router()

const {
	authenticateToken,
	authenticatePost,
	authenticateGroup,
	authenticateAdmin,
} = require('../middlewares/auth')

tagRoutes.get('/', TagController.getTag)
tagRoutes.get('/count/group', TagController.getGroupCount)
tagRoutes.get('/count/post', TagController.getPostCount)
tagRoutes.get('/count/user', TagController.getUserCount)
tagRoutes.post('/', authenticateToken, TagController.create)
tagRoutes.put(
	'/group/:groupName',
	authenticateToken,
	authenticateGroup,
	TagController.addGroupTag
)
tagRoutes.put(
	'/post/:postId',
	authenticateToken,
	authenticatePost,
	TagController.addPostTag
)
tagRoutes.delete(
	'/:name',
	authenticateToken,
	authenticateAdmin,
	TagController.delete
)

module.exports = tagRoutes
