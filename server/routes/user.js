const express = require('express')

const CredentialController = require('../controllers/credential')
const FavoriteController = require('../controllers/favorite')
const FollowerController = require('../controllers/follower')
const ProfileController = require('../controllers/profile')
const UserController = require('../controllers/user')

const { authenticateToken, authenticateAdmin } = require('../middlewares/auth')

const userRoutes = express.Router()

// Follower route
userRoutes.get('/follower/:userId', FollowerController.getFollower)
userRoutes.get('/followed/:userId', FollowerController.getFollowed)
userRoutes.get('/follower/count/:userId', FollowerController.count)
userRoutes.get(
	'/follower/:followerId/:followedId',
	FollowerController.isFollowedBy
)
userRoutes.post(
	'/follower/:userId/:followedId',
	authenticateToken,
	FollowerController.create
)
userRoutes.delete(
	'/follower/:userId/:followedId',
	authenticateToken,
	FollowerController.delete
)

// Favorite route
userRoutes.get(
	'/favorite/:userId',
	authenticateToken,
	FavoriteController.getFavorite
)
userRoutes.get(
	'/favorite/:userId/:postId',
	authenticateToken,
	FavoriteController.getById
)
userRoutes.post(
	'/favorite/:userId/:postId',
	authenticateToken,
	FavoriteController.create
)
userRoutes.delete(
	'/favorite/:userId/:postId',
	authenticateToken,
	FavoriteController.delete
)

// Profile route
userRoutes.get('/profile/:userId', ProfileController.getProfile)
userRoutes.post('/profile/:userId', authenticateToken, ProfileController.create)
userRoutes.put('/profile/:userId', authenticateToken, ProfileController.update)
userRoutes.delete(
	'/profile/:userId',
	authenticateToken,
	ProfileController.delete
)

// Main user route
userRoutes.get(
	'/',
	authenticateToken,
	authenticateAdmin,
	UserController.getUser
)
userRoutes.get('/:userId', UserController.getById)
userRoutes.get('/:userId/tags', authenticateToken, UserController.getTags)
userRoutes.post('/register', UserController.create)
userRoutes.put('/:userId', authenticateToken, UserController.update)
userRoutes.delete('/:userId', authenticateToken, UserController.deleteUser)

userRoutes.get('/:userId/errors', CredentialController.getErrors)
userRoutes.post('/login', CredentialController.login)
userRoutes.post(
	'/:userId/change-password',
	authenticateToken,
	CredentialController.changePassword
)

module.exports = userRoutes
