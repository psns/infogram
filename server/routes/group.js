const express = require('express')
const GroupController = require('../controllers/group')
const { authenticateToken, authenticateGroup } = require('../middlewares/auth')

const groupRoutes = express.Router()

groupRoutes.get('/', GroupController.getGroup)
groupRoutes.get('/:groupName', GroupController.getById)
groupRoutes.post('/', authenticateToken, GroupController.create)
groupRoutes.get(
	'/user/:userId',
	authenticateToken,
	GroupController.getUserGroup
)
groupRoutes.put(
	'/:groupName',
	authenticateToken,
	authenticateGroup,
	GroupController.update
)
groupRoutes.delete(
	'/:groupName',
	authenticateToken,
	authenticateGroup,
	GroupController.delete
)

module.exports = groupRoutes
