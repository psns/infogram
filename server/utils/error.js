const { PrismaClientValidationError } = require('@prisma/client/runtime')

/**
 *
 * @param {Response} res
 * @param {Error} error
 */
module.exports.handleError = (res, error) => {
	console.log(error)
	if (error instanceof PrismaClientValidationError) {
		res.status(400).send({
			error: 'P2012',
			message: 'Error: constraint validation error',
		})
	} else {
		switch (error.code) {
			case 'P2002':
				res.status(400).send({
					code: error.code,
					meta: error.meta,
					message: 'Error: unique constraint failed',
				})
				break
			case 'P2003':
				res.status(400).send({
					code: error.code,
					meta: error.meta,
					message: 'Error: foreign constraint failed',
				})
				break
			case 'P2004':
				res.status(400).send({
					code: error.code,
					meta: error.meta,
					message: 'Error: constraint failed',
				})
				break
			case 'P2005':
				res.status(400).send({
					code: error.code,
					meta: error.meta,
					message: 'Error: field type failed',
				})
				break
			case 'P2011':
				res.status(400).send({
					code: error.code,
					meta: error.meta,
					message: 'Error: null constraint failed',
				})
				break
			case 'P2014':
				res.status(400).send({
					code: error.code,
					meta: error.meta,
					message: 'Error: relation violation',
				})
				break
			case 'P2022':
				res.status(400).send({
					code: error.code,
					meta: error.meta,
					message: 'Error: column not exist',
				})
				break
			case 'P2025':
				res.status(400).send({
					code: error.code,
					meta: error.meta,
					message: 'Error: record relation not found',
				})
				break
			default:
				res.status(500).send({
					message: 'Error: Server error',
				})
				break
		}
	}
}
