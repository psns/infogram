const { PrismaClient } = require('@prisma/client')
const { promiseToSortArray } = require('../utils/promise')

const prisma = new PrismaClient()

class TagService {
	/**
	 * Get all tags
	 *
	 * @returns Tag[]
	 */
	static async #getAllTag() {
		return prisma.tag.findMany()
	}

	/**
	 * Get tag by his name
	 *
	 * @param {string} tag
	 * @returns Tag
	 */
	static async getTagByName(tag) {
		return prisma.tag.findUnique({
			where: {
				name: tag,
			},
		})
	}

	/**
	 * Get page of tags
	 *
	 * @returns Tag[]
	 */
	static async getTag(page, limit) {
		return prisma.tag.findMany({
			take: limit,
			skip: page * limit,
		})
	}

	/**
	 * Get the use of tag in groups
	 *
	 * @returns any[]
	 */
	static async getGroupCount() {
		const tags = await this.#getAllTag()

		const tagMap = await tags.map(tag => {
			return prisma.group
				.count({
					where: {
						Tags: {
							some: {
								name: tag.name,
							},
						},
					},
				})
				.then(res => {
					return { tag: tag.name, count: res }
				})
		})
		return await promiseToSortArray(tagMap)
	}

	/**
	 * Get the use of tag in post
	 *
	 * @returns any[]
	 */
	static async getPostCount() {
		const tags = await this.#getAllTag()

		const tagMap = await tags.map(tag => {
			return prisma.post
				.count({
					where: {
						Tags: {
							some: {
								name: tag.name,
							},
						},
					},
				})
				.then(res => {
					return { tag: tag.name, count: res }
				})
		})
		return await promiseToSortArray(tagMap)
	}

	/**
	 * Get the use of tag forn user
	 *
	 * @returns any[]
	 */
	static async getUserCount() {
		const tags = await this.#getAllTag()

		const tagMap = await tags.map(tag => {
			return prisma.user
				.count({
					where: {
						Tags: {
							some: {
								name: tag.name,
							},
						},
					},
				})
				.then(res => {
					return { tag: tag.name, count: res }
				})
		})
		return await promiseToSortArray(tagMap)
	}

	/**
	 * Create new tag
	 *
	 * @param {string} name
	 * @returns Tag
	 */
	static async create(name) {
		return await prisma.tag.create({
			data: {
				name: name,
			},
		})
	}

	/**
	 * Add tags to a post
	 *
	 * @param {string} id
	 * @param {Tag[]} tags
	 */
	static async addPostTag(id, tags) {
		tags.forEach(async tag => {
			await prisma.post.update({
				where: {
					id: id,
				},
				data: {
					Tags: {
						connectOrCreate: {
							where: {
								name: tag.name,
							},
							create: {
								name: tag.name,
							},
						},
					},
				},
			})
		})
	}

	/**
	 * Add tags to the group
	 *
	 * @param {string} groupName
	 * @param {Tag[]} tags
	 */
	static async addGroupTag(name, tags) {
		return await prisma.group.update({
			where: {
				name: name,
			},
			data: {
				Tags: {
					connect: tags,
				},
			},
		})
	}

	/**
	 * Delete a tag
	 *
	 * @param {string} name
	 * @returns Tag
	 */
	static async delete(name) {
		return await prisma.tag.delete({
			where: {
				name: name,
			},
		})
	}
}

module.exports = TagService
