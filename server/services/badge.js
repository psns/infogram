const { PrismaClient } = require('.prisma/client')
const { promiseToSortArray } = require('../utils/promise')

const prisma = new PrismaClient()

class BadgeService {
	/**
	 * Get all badge
	 *
	 * @returns Badge
	 */
	static async getAll() {
		return await prisma.badge.findMany()
	}

	/**
	 * Get badges for post
	 *
	 * @param {string} postId
	 */
	static async getBadgePost(postId) {
		const badges = await this.getAll()

		const badgeMap = await badges.map(badge => {
			return prisma.badgePost
				.count({
					where: {
						badgeId: badge.id,
						postId: postId,
					},
				})
				.then(res => {
					return { name: badge.name, count: res }
				})
		})
		return await promiseToSortArray(badgeMap)
	}

	/**
	 * Create new badge
	 *
	 * @param {string} name
	 * @param {string} icon
	 * @returns Badge
	 */
	static async create(name, icon) {
		return await prisma.badge.create({
			data: {
				name: name,
				icon: icon,
			},
		})
	}

	/**
	 * Assign a badge to a post
	 *
	 * @param {string} userId
	 * @param {string} postId
	 * @param {number} badgeId
	 * @returns BadgePost
	 */
	static async assign(userId, postId, badgeId) {
		return await prisma.badgePost.create({
			data: {
				userId: userId,
				postId: postId,
				badgeId: badgeId,
			},
		})
	}

	/**
	 * Unassign a badge to a post
	 *
	 * @param {string} userId
	 * @param {string} postId
	 * @param {number} badgeId
	 * @returns BadgePost
	 */
	static async unasign(userId, postId, badgeId) {
		return await prisma.badgePost.delete({
			where: {
				userId_postId_badgeId: {
					userId: userId,
					postId: postId,
					badgeId: badgeId,
				},
			},
		})
	}

	/**
	 * Update a badge
	 *
	 * @param {number} id
	 * @param {string} name
	 * @param {string} icon
	 * @returns Badge
	 */
	static async update(id, name, icon) {
		return await prisma.badge.update({
			where: {
				id: id,
			},
			data: {
				name: name,
				icon: icon,
			},
		})
	}

	/**
	 * Delete a badge
	 *
	 * @param {number} id
	 * @returns Badge
	 */
	static async delete(id) {
		return await prisma.badge.delete({
			where: {
				id: id,
			},
		})
	}
}

module.exports = BadgeService
