const { PrismaClient } = require('.prisma/client')

const prisma = new PrismaClient()

class ScoreService {
	/**
	 * Get score of post
	 *
	 * @param {string} postId
	 * @returns Score
	 */
	static async getScore(postId) {
		return await prisma.score.aggregate({
			where: {
				postId: postId,
			},
			sum: {
				value: true,
			},
		})
	}

	/**
	 * Get user score for a post
	 *
	 * @param {string} userId
	 * @param {string} postId
	 * @returns Score
	 */
	static async getUserScore(userId, postId) {
		return await prisma.score.findUnique({
			where: {
				userId_postId: {
					postId: postId,
					userId: userId,
				},
			},
		})
	}

	/**
	 * Assign news score
	 *
	 * @param {string} userId
	 * @param {string} postId
	 * @param {number} value
	 * @returns Score
	 */
	static async assign(userId, postId, value) {
		return await prisma.score.upsert({
			where: {
				userId_postId: {
					postId: postId,
					userId: userId,
				},
			},
			create: {
				Post: {
					connect: {
						id: postId,
					},
				},
				User: {
					connect: {
						id: userId,
					},
				},
				value: value,
			},
			update: {
				value: value,
			},
		})
	}
}

module.exports = ScoreService
