const { PrismaClient } = require('.prisma/client')

const prisma = new PrismaClient()

class FollowerService {
	/**
	 * Get followed of user
	 *
	 * @param {string} id
	 * @returns User
	 */
	static async getFollowed(id, page, limit) {
		return await prisma.user.findMany({
			select: {
				id: true,
				nickname: true,
			},
			where: {
				Followers: {
					some: {
						followedId: id,
					},
				},
			},
			orderBy: {
				nickname: 'asc',
			},
			skip: page * limit,
			take: limit,
		})
	}

	/**
	 * Check if an user follow another
	 *
	 * @param {string} followerId
	 * @param {string} followedId
	 * @returns Follower
	 */
	static async isFollowedBy(followerId, followedId) {
		return await prisma.followers.findUnique({
			where: {
				followerId_followedId: {
					followerId: followerId,
					followedId: followedId,
				},
			},
		})
	}

	/**
	 * Get follower of user
	 *
	 * @param {string} id
	 * @returns User
	 */
	static async getFollower(id, page, limit) {
		return await prisma.user.findMany({
			select: {
				id: true,
				nickname: true,
			},
			where: {
				Followed: {
					some: {
						followerId: id,
					},
				},
			},
			orderBy: {
				nickname: 'asc',
			},
			skip: page * limit,
			take: limit,
		})
	}

	/**
	 * Get number of follower, followed
	 *
	 * @param {string} id
	 * @returns any
	 */
	static async count(id) {
		const follower = await prisma.followers.count({
			where: {
				followerId: id,
			},
		})
		const followed = await prisma.followers.count({
			where: {
				followedId: id,
			},
		})
		return { follower: follower, followed: followed }
	}

	/**
	 * Create new follower relation
	 *
	 * @param {string} userId
	 * @param {string} followedId
	 * @returns Follower
	 */
	static async create(userId, followedId) {
		return await prisma.followers.create({
			data: {
				followerId: userId,
				followedId: followedId,
			},
		})
	}

	/**
	 * Delete follower relation
	 *
	 * @param {string} userId
	 * @param {string} followedId
	 * @returns
	 */
	static async delete(userId, followedId) {
		return await prisma.followers.delete({
			where: {
				followerId_followedId: {
					followerId: userId,
					followedId: followedId,
				},
			},
		})
	}

	static async deleteAll(userId) {
		return await prisma.followers.deleteMany({
			where: {
				OR: [
					{
						followedId: userId,
					},
					{
						followerId: userId,
					},
				],
			},
		})
	}
}

module.exports = FollowerService
