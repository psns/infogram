const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

class GroupService {
	/**
	 * Get page of groups
	 *
	 * @returns Group[]
	 */
	static async getGroup(page, limit) {
		return await prisma.group.findMany({
			take: limit,
			skip: page * limit,
		})
	}

	/**
	 * Get a group by his name
	 *
	 * @param {string} name
	 * @returns Group
	 */
	static async getById(name) {
		return await prisma.group.findUnique({
			where: {
				name: name,
			},
			include: {
				Tags: true,
			},
		})
	}

	/**
	 * Get all groups for a user
	 *
	 * @param {string} userId
	 * @returns string[]
	 */
	static async getUserGroups(userId) {
		return await prisma.group.findMany({
			select: {
				name: true,
			},
			where: {
				GroupMember: {
					some: {
						userId: userId,
					},
				},
			},
		})
	}

	/**
	 * Create a group
	 *
	 * @param {string} name
	 * @param {string} owner
	 * @param {string} description
	 * @returns string
	 */
	static async create(name, owner, description) {
		const group = await prisma.group.create({
			data: {
				name: name,
				description: description,
				Owner: {
					connect: {
						id: owner,
					},
				},
				GroupMember: {
					create: {
						User: {
							connect: {
								id: owner,
							},
						},
					},
				},
			},
		})
		return group.name
	}

	/**
	 * Update a group
	 *
	 * @param {string} owner
	 * @param {string} description
	 * @returns string
	 */
	static async update(name, owner, description, path) {
		const group = await prisma.group.update({
			where: {
				name: name,
			},
			data: {
				description: description,
				backgroundImage: path,
				Owner: {
					connect: {
						id: owner,
					},
				},
			},
		})
		return group.name
	}

	/**
	 * Delete a group
	 *
	 * @param {string} name
	 * @returns Group
	 */
	static async delete(name) {
		await prisma.groupMember.deleteMany({
			where: {
				groupId: name,
			},
		})
		return await prisma.group.delete({
			where: {
				name: name,
			},
		})
	}
}

module.exports = GroupService
