const { PrismaClient } = require('@prisma/client')
const argon2id = require('argon2')
const prisma = new PrismaClient()

class CredentialService {
	/**
	 * Check if credential is correct for an user
	 *
	 * @param {string} login
	 * @param {string} password
	 */
	static async login(login, password) {
		const credential = await prisma.credential.findFirst({
			where: {
				user: {
					OR: [
						{
							email: login,
						},
						{
							phone: login,
						},
					],
				},
			},
		})
		if (!credential) {
			return false
		}
		const correct = await argon2id.verify(credential.hash, password)
		const result = await prisma.credential.update({
			select: {
				user: {
					select: {
						id: true,
						nickname: true,
					},
				},
			},
			where: {
				userId: credential.userId,
			},
			data: {
				error: correct ? 0 : ++credential.error,
			},
		})
		return correct ? result : correct
	}

	/**
	 * Create new credential for user
	 *
	 * @param {string} userId
	 * @param {string} password
	 * @returns Credential
	 */
	static async createCredential(userId, password) {
		const hash = await argon2id.hash(password)
		return await prisma.credential.create({
			data: {
				hash: hash,
				user: {
					connect: {
						id: userId,
					},
				},
			},
		})
	}

	/**
	 * Change user password
	 *
	 * @param {string} userId
	 * @param {string} password
	 */
	static async changePassword(userId, password) {
		const hash = await argon2id.hash(password)

		await prisma.credential.update({
			where: {
				userId: userId,
			},
			data: {
				hash: hash,
			},
		})
	}

	/**
	 * Get crendetial by his id
	 *
	 * @param {string} userId
	 * @returns Credential
	 */
	static async getById(userId) {
		return await prisma.credential.findUnique({
			where: {
				userId: userId,
			},
		})
	}

	/**
	 * Delete credential
	 *
	 * @param {string} userId
	 * @returns Credential
	 */
	static async delete(userId) {
		return await prisma.credential.delete({
			where: {
				userId: userId,
			},
		})
	}
}

module.exports = CredentialService
