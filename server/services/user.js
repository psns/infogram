const { PrismaClient } = require('@prisma/client')
const CredentialService = require('./credential')
const FollowerService = require('./follower')
const ProfileService = require('./profile')

const prisma = new PrismaClient()

class UserService {
	/**
	 * Get page of users
	 *
	 * @returns User[]
	 */
	static async getUser(page, limit) {
		return await prisma.user.findMany({
			skip: page * limit,
			take: limit,
		})
	}

	/**
	 * Get user by his id
	 *
	 * @param {string} id
	 * @returns User
	 */
	static async getById(id) {
		return await prisma.user.findFirst({
			where: {
				id: id,
			},
		})
	}

	/**
	 * Get user tags
	 *
	 * @param {string} userId
	 * @returns Tag[]
	 */
	static async getTags(userId) {
		return await prisma.user.findMany({
			select: {
				Tags: {
					select: {
						name: true,
					},
				},
			},
			where: {
				id: userId,
			},
		})
	}

	/**
	 *
	 * @param {User} user
	 * @param {string} password
	 * @returns
	 */
	static async create(user, password) {
		const { email, name, nickname, phone, birth } = user

		const result = await prisma.user.create({
			select: {
				id: true,
			},
			data: {
				email: email,
				name: name,
				nickname: nickname,
				phone: phone,
				birth: birth,
			},
		})

		await CredentialService.createCredential(result.id, password)
		await ProfileService.create(result.id)

		return result
	}

	/**
	 * Update an user
	 *
	 * @param {string} id
	 * @param {User} user
	 * @param {string} password
	 * @param {string[]} tags
	 */
	static async update(id, user, tags) {
		const { email, name, nickname, phone, birth } = user

		if (tags !== undefined && tags instanceof Array) {
			await tags.forEach(async tag => {
				await prisma.user.update({
					where: {
						id: id,
					},
					data: {
						Tags: {
							connect: {
								name: tag,
							},
						},
					},
				})
			})
		}

		const result = await prisma.user.update({
			where: {
				id: id,
			},
			data: {
				email: email,
				name: name,
				nickname: nickname,
				phone: phone,
				birth: birth,
			},
		})
		return result
	}

	/**
	 * Delete an user
	 *
	 * @param {string} userId
	 */
	static async delete(userId) {
		const user = await this.getById(userId)
		if (user) {
			await FollowerService.deleteAll(userId)
			await ProfileService.delete(userId)
			await CredentialService.delete(userId)

			return await prisma.user.delete({
				where: {
					id: userId,
				},
			})
		}
		return user
	}
}

module.exports = UserService
