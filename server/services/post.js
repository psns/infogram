const { PrismaClient } = require('@prisma/client')
const { promiseToSortArray } = require('../utils/promise')
const prisma = new PrismaClient()

class PostService {
	/**
	 * Get post by his id
	 *
	 * @param {sting} postId
	 * @returns
	 */
	static async getPost(postId) {
		return await prisma.post.findFirst({
			where: {
				id: postId,
			},
			include: {
				Tags: true,
			},
		})
	}

	/**
	 * Get page of popular posts
	 *
	 * @param {string} until
	 * @param {number} page
	 * @param {number} limit
	 * @returns Any[]
	 */
	static async getPopularPage(until, page, limit) {
		const now = new Date().toISOString().replace('T', ' ').replace('Z', '')
		const untilDate = until.replace('T', ' ').replace('Z', '')
		const query = `
		SELECT p.id, SUM(COALESCE(value, 0)) as counts
		FROM public."Post" as p LEFT JOIN public."Score" as s ON p.id = s."postId"
		WHERE p."createdAt" BETWEEN '${untilDate}' AND '${now}'
		GROUP BY p.id
		ORDER BY counts DESC, p."createdAt" DESC
		LIMIT ${limit} OFFSET ${page * limit}
		`

		const posts = await prisma.$queryRaw(query)
		const postMap = await posts.map(post => {
			return prisma.post
				.findUnique({
					where: {
						id: post.id,
					},
					include: {
						Tags: true,
					},
				})
				.then(res => {
					return res
				})
		})
		return await promiseToSortArray(postMap)
	}

	/**
	 * Get page of tag posts
	 *
	 * @param {string} tag
	 * @param {string} until
	 * @param {number} page
	 * @param {number} limit
	 */
	static async getTagsPost(tag, until, page, limit) {
		const now = new Date().toISOString().replace('T', ' ').replace('Z', '')
		const untilDate = until.replace('T', ' ').replace('Z', '')
		const query = `
		SELECT p.id, SUM(COALESCE(value, 0)) as counts
		FROM public."Post" as p LEFT JOIN public."Score" as s ON p.id = s."postId"
		INNER JOIN public."_PostToTag" as t ON p.id = t."A"
		WHERE t."B" = '${tag}' AND p."createdAt" BETWEEN '${untilDate}' AND '${now}'
		GROUP BY p.id
		ORDER BY counts DESC, p."createdAt" DESC
		LIMIT ${limit} OFFSET ${page * limit}
		`

		const posts = await prisma.$queryRaw(query)
		const postMap = await posts.map(post => {
			return prisma.post
				.findUnique({
					where: {
						id: post.id,
					},
					include: {
						Tags: true,
					},
				})
				.then(res => {
					return res
				})
		})
		return await promiseToSortArray(postMap)
	}

	/**
	 * Get page of follower posts
	 *
	 * @param {string} userId
	 * @param {number} page
	 * @param {number} limit
	 */
	static async getFollowerPage(userId, page, limit) {
		return await prisma.post.findMany({
			where: {
				Publisher: {
					Followed: {
						some: {
							followerId: userId,
						},
					},
				},
			},
			orderBy: {
				createdAt: 'desc',
			},
			include: {
				Tags: true,
			},
			skip: limit * page,
			take: limit,
		})
	}

	/**
	 * Get page of posts of user
	 *
	 * @param {string} 444
	 * @param {number} page
	 * @param {number} limit
	 * @returns Post[]
	 */
	static async getPage(userId, page, limit) {
		return await prisma.post.findMany({
			where: {
				publisherId: userId,
			},
			include: {
				Tags: true,
			},
			orderBy: {
				createdAt: 'desc',
			},
			skip: limit * page,
			take: limit,
		})
	}

	/**
	 * Get page of response's post
	 *
	 * @param {string} postId
	 * @param {number} nbPage
	 * @param {number} nbElem
	 * @returns Post[]
	 */
	static async getResponse(postId, nbPage, nbElem) {
		return await prisma.post.findMany({
			where: {
				parentId: postId,
			},
			orderBy: {
				createdAt: 'asc',
			},
			include: {
				Tags: true,
			},
			skip: nbElem * nbPage,
			take: nbElem,
		})
	}

	/**
	 * Get nb of reponse
	 *
	 * @param {string} postId
	 */
	static async getNbResponse(postId) {
		return await prisma.post.aggregate({
			where: {
				parentId: postId,
			},
			count: true,
		})
	}

	/**
	 * Create new post
	 *
	 * @param {string} userId
	 * @param {string} description
	 * @param {*} postType
	 * @param {string} parentId
	 * @param {JSON} content
	 * @returns Post
	 */
	static async create(
		userId,
		description,
		postType,
		content = null,
		parentId = null
	) {
		return await prisma.post.create({
			select: {
				id: true,
			},
			data: {
				publisherId: userId,
				parentId: parentId,
				description: description,
				content: content,
				type: postType,
			},
		})
	}

	/**
	 * Update content in post
	 *
	 * @param {string} postId
	 * @param {JSON} content
	 * @returns Post
	 */
	static async updateContent(postId, content) {
		return await prisma.post.update({
			where: {
				id: postId,
			},
			data: {
				content: content,
			},
		})
	}

	/**
	 * Delete a post
	 *
	 * @param {string} postId
	 * @returns Post
	 */
	static async delete(postId) {
		await prisma.post.update({
			where: {
				id: postId,
			},
			data: {
				Favorite: {
					deleteMany: {
						postId: postId,
					},
				},
				BadgePost: {
					deleteMany: {
						postId: postId,
					},
				},
				Scores: {
					deleteMany: {
						postId: postId,
					},
				},
			},
		})
		return await prisma.post.delete({
			where: {
				id: postId,
			},
		})
	}
}

module.exports = PostService
