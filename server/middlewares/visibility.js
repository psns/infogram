const FollowerService = require('../services/follower')
const GroupService = require('../services/group')
const { Visibility } = require('.prisma/client')

/**
 * Check if object contain visibility
 *
 * @param {any} obj
 */
const isNotPublic = obj => {
	if (
		Object.keys(obj).includes('visibility') &&
		obj.visibility !== Visibility.PUBLIC
	) {
		return true
	}
	return false
}

/**
 * Check if user his authorized for group
 *
 * @param {Response} res
 * @param {string} userId
 * @param {Group} group
 */
module.exports.GroupVisibility = async (res, userId, group) => {
	if (isNotPublic()) {
		const groups = await GroupService.getUserGroups(userId)
		if (groups.find(grp => grp.name === group.name) === undefined) {
			res.status(403).send()
			return
		}
	}
	res.json(group)
}

/**
 * Check if user his authorized for profile
 *
 * @param {Response} res
 * @param {string} userId
 * @param {Profile} profile
 */
module.exports.ProfileVisibility = async (res, userId, profile) => {
	if (isNotPublic()) {
		if (
			profile.visibility === Visibility.PRIVATE &&
			profile.userId !== userId
		) {
			res.status(403).send()
			return
		}
		const follow = await FollowerService.isFollowedBy(userId, profile.userId)
		if (!follow) {
			res.status(403).send()
			return
		}
	}
	res.json(profile)
}
