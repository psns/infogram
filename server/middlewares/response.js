module.exports.NotFound = (req, res, next) => {
	res.status(404)

	// respond with json
	if (req.accepts('json')) {
		res.json({ message: 'Not found' })
		return
	}

	// default to plain-text. send()
	res.type('txt').send('Not found')
}

module.exports.ServerError = (err, req, res, next) => {
	res.status(err.status || 500)
	res.json({
		message: 'Error: Server error',
		error: err.message,
	})
}
