const PostService = require('../services/post')
const ScoreService = require('../services/score')
const { handleError } = require('../utils/error')

class ScoreController {
	/**
	 * Get score of post
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getScore(req, res) {
		try {
			const { postId } = req.params
			const post = await PostService.getPost(postId)
			if (post) {
				const score = await ScoreService.getScore(postId)
				res.json(score)
				return
			}
			res.send()
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get user score for a post
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getUserScore(req, res) {
		try {
			const { userId, postId } = req.params
			const post = await PostService.getPost(postId)
			if (post) {
				const score = await ScoreService.getUserScore(userId, postId)
				res.json(score)
				return
			}
			res.send()
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Assign score of post
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async assign(req, res) {
		try {
			const { userId } = req.params
			const { postId, value } = req.body
			const score = await ScoreService.assign(userId, postId, value)
			res.json(score)
		} catch (error) {
			handleError(res, error)
		}
	}
}

module.exports = ScoreController
