const FavoriteService = require('../services/favorite')
const { handleError } = require('../utils/error')

class FavoriteController {
	/**
	 * Get all favorites
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getFavorite(req, res) {
		try {
			const { userId } = req.params
			const { page, limit } = req.query

			const nbPage = page ? Number.parseInt(page) : 0
			const nbLimit = limit ? Number.parseInt(limit) : 25
			const favorites = await FavoriteService.getFavorite(
				userId,
				nbPage,
				nbLimit
			)
			res.json(favorites)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get favorite by idsgetFavorite
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getById(req, res) {
		try {
			const { userId, postId } = req.params
			const favorite = await FavoriteService.getById(userId, postId)
			res.json(favorite)
		} catch (error) {
			handleError(error)
		}
	}

	/**
	 * Create new favorite
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async create(req, res) {
		try {
			const { userId, postId } = req.params
			const favorite = await FavoriteService.create(userId, postId)
			res.json(favorite)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Delete favorite
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async delete(req, res) {
		try {
			const { userId, postId } = req.params
			const favorite = await FavoriteService.delete(userId, postId)
			res.json(favorite)
		} catch (error) {
			handleError(res, error)
		}
	}
}

module.exports = FavoriteController
