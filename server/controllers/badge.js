const BadgeService = require('../services/badge')
const { handleError } = require('../utils/error')

class BadgeController {
	/**
	 * Get all badge
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getAll(req, res) {
		try {
			const badges = await BadgeService.getAll()
			res.json(badges)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get badges for post
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getBadgePost(req, res) {
		try {
			const { postId } = req.params
			const badges = await BadgeService.getBadgePost(postId)
			res.json(badges)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Create new badge
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async create(req, res) {
		try {
			const { name, icon } = req.body
			const badge = await BadgeService.create(name, icon)
			res.status(201).json(badge)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Update a badge
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async update(req, res) {
		try {
			const { badgeId } = req.params
			const { name, icon } = req.body
			const id = Number.parseInt(badgeId)
			const badge = await BadgeService.update(id, name, icon)
			res.json(badge)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Delete a badge
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async delete(req, res) {
		try {
			const { badgeId } = req.params
			const id = Number.parseInt(badgeId)
			const badge = await BadgeService.delete(id)
			res.json(badge)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Assign a badge to a post
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async assign(req, res) {
		try {
			const { userId } = req.params
			const { postId, badgeId } = req.body
			const id = Number.parseInt(badgeId)
			const badgePost = await BadgeService.assign(userId, postId, id)
			res.status(201).json(badgePost)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Unassign a badge to a post
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async unassign(req, res) {
		try {
			const { userId } = req.params
			const { postId, badgeId } = req.body
			const id = Number.parseInt(badgeId)
			const badgePost = await BadgeService.unasign(userId, postId, id)
			res.json(badgePost)
		} catch (error) {
			handleError(res, error)
		}
	}
}

module.exports = BadgeController
