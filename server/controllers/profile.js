const ProfileService = require('../services/profile')
const { handleError } = require('../utils/error')

class ProfileController {
	/**
	 * Get an user's profile by userId
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getProfile(req, res) {
		try {
			const { userId } = req.params
			const profile = await ProfileService.getProfile(userId)
			res.json(profile)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Create an user's profile
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async create(req, res) {
		try {
			const { userId } = req.params
			const { description, image, backgroundImage } = req.body
			const profile = await ProfileService.create(
				userId,
				description,
				image,
				backgroundImage
			)
			res.status(201).json(profile)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Update an user's profile
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async update(req, res) {
		try {
			const { userId } = req.params
			const { description, image, backgroundImage, visibility } = req.body
			const profile = await ProfileService.update(
				userId,
				description,
				image,
				backgroundImage,
				visibility
			)
			res.json(profile)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Delete an user's profile
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async delete(req, res) {
		try {
			const { userId } = req.params
			const profile = await ProfileService.delete(userId)
			res.json(profile)
		} catch (error) {
			handleError(res, error)
		}
	}
}

module.exports = ProfileController
