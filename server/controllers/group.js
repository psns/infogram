const GroupService = require('../services/group')
const { handleError } = require('../utils/error')

class GroupController {
	/**
	 * Get page groups
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getGroup(req, res) {
		try {
			const { page, limit } = req.query

			const nbPage = page ? Number.parseInt(page) : 0
			const nbLimit = limit ? Number.parseInt(limit) : 25
			const groups = await GroupService.getGroup(nbPage, nbLimit)
			res.json(groups)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get a group by his id
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getById(req, res) {
		try {
			const { groupName } = req.params
			const group = await GroupService.getById(groupName)
			res.json(group)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get group of user
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getUserGroup(req, res) {
		try {
			const { userId } = req.params
			const groups = await GroupService.getUserGroups(userId)
			res.json(groups)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Create a group
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async create(req, res) {
		try {
			const { name, owner, description } = req.body
			const groupName = await GroupService.create(name, owner, description)
			res.status(201).json(groupName)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Update a group
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async update(req, res) {
		try {
			const { groupName } = req.params
			const { owner, description } = req.body
			const result = await GroupService.update(groupName, owner, description)
			res.json(result)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Delete a group
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async delete(req, res) {
		try {
			const { groupName } = req.params
			const result = await GroupService.delete(groupName)
			res.json(result)
		} catch (error) {
			handleError(res, error)
		}
	}
}

module.exports = GroupController
