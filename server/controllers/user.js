const UserService = require('../services/user')
const { handleError } = require('../utils/error')

class UserController {
	/**
	 * Get all users
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getUser(req, res) {
		try {
			const { page, limit } = req.query

			const nbPage = page ? Number.parseInt(page) : 0
			const nbLimit = limit ? Number.parseInt(limit) : 25
			const result = await UserService.getUser(nbPage, nbLimit)
			res.json(result)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get user by id
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getById(req, res) {
		try {
			const { userId } = req.params
			const result = await UserService.getById(userId)
			res.json(result)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Create an user
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async create(req, res) {
		try {
			const { user, password } = req.body
			const userId = await UserService.create(user, password)
			res.status(201).json(userId)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get user tags
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getTags(req, res) {
		try {
			const { userId } = req.params
			const tags = await UserService.getTags(userId)
			res.json(tags)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Update an user
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async update(req, res) {
		try {
			const { userId } = req.params
			const { user, tags } = req.body

			const result = await UserService.update(userId, user, tags)

			res.json(result)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * delete an user
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async deleteUser(req, res) {
		try {
			const { userId } = req.params
			const result = await UserService.delete(userId)
			res.json(result)
		} catch (error) {
			handleError(res, error)
		}
	}
}

module.exports = UserController
