const TagService = require('../services/tag')
const { handleError } = require('../utils/error')

class TagController {
	/**
	 * Get page of tags
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getTag(req, res) {
		try {
			const { page, limit } = req.query

			const nbPage = page ? Number.parseInt(page) : 0
			const nbLimit = limit ? Number.parseInt(limit) : 25
			const tags = await TagService.getTag(nbPage, nbLimit)
			res.send(tags)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get the use of a tag
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getGroupCount(req, res) {
		try {
			const tags = await TagService.getGroupCount()
			res.send(tags)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get the use of a tag
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getPostCount(req, res) {
		try {
			const tags = await TagService.getPostCount()
			res.send(tags)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get the use of a tag
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getUserCount(req, res) {
		try {
			const tags = await TagService.getUserCount()
			res.send(tags)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Create new tag
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async create(req, res) {
		try {
			const { name } = req.body
			const tags = await TagService.create(name)
			res.send(tags)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Add tags to the post
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async addPostTag(req, res) {
		try {
			const { postId } = req.params
			const { tags } = req.body
			const post = await TagService.addPostTag(postId, tags)
			res.json(post)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Add tags to the group
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async addGroupTag(req, res) {
		try {
			const { groupName } = req.params
			const { tags } = req.body
			const group = await TagService.addGroupTag(groupName, tags)
			res.json(group)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Delete a tag
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async delete(req, res) {
		try {
			const { name } = req.params
			const tags = await TagService.delete(name)
			res.send(tags)
		} catch (error) {
			handleError(res, error)
		}
	}
}

module.exports = TagController
