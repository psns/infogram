const express = require('express')
const cookieParser = require('cookie-parser')
const cors = require('cors')
const fileUpload = require('express-fileupload')
const fs = require('fs-extra')
const path = require('path')

const userRoutes = require('./routes/user')
const groupRoutes = require('./routes/group')
const postRoutes = require('./routes/post')
const messageRoutes = require('./routes/message')
const tagRoutes = require('./routes/tag')
const filerRoutes = require('./routes/filer')
const { NotFound, ServerError } = require('./middlewares/response')

const app = express()
app.use(express.json())
app.use(cookieParser())
app.use(
	fileUpload({
		createParentPath: true,
	})
)
app.use(
	cors({
		origin: 'http://localhost:8000',
		credentials: true,
	})
)

const root = express.Router()

root.get('/', (req, res) => {
	res.json('Hello World !')
})

root.use('/user', userRoutes)
root.use('/group', groupRoutes)
root.use('/post', postRoutes)
root.use('/message', messageRoutes)
root.use('/tag', tagRoutes)
root.use('/filer', filerRoutes)

app.use('/api', root)

try {
	fs.statSync(path.join(__dirname, '../build'))
	app.use(express.static(path.join(__dirname, '../build')))
	app.get('*', (req, res) => {
		res.sendFile(path.join(__dirname, '../build', 'index.html'))
	})
} catch {
	console.log(
		'No build directory detected, unable to serve app, you must compile React app first '
	)
}

app.use(NotFound)
app.use(ServerError)

const HTTP_PORT = 3000
app.listen(HTTP_PORT, () =>
	console.log(`Server ready at: http://localhost:${HTTP_PORT}`)
)
