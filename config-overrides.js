const { alias } = require('react-app-rewire-alias')

module.exports = function override(config) {
	alias({
		'@': 'src',
		'@views': 'src/views',
		'@utils': 'src/utils',
		'@components': 'src/components',
		'@assets': 'src/assets',
		'@fonts': 'src/fonts',
		'@themes': 'src/themes',
		'@services': 'src/services',
		'@contexts': 'src/contexts',
	})(config)

	return config
}
