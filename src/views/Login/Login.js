import { useContext, useState } from 'react'
import { useHistory } from 'react-router'

// MUI core Imports
import { Button, Divider, Link, makeStyles } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'

// Infogram contexts
import AppContext from '@contexts/AppContext'

// Infogram services
import { UserService } from '@services/user'

// Infogram custom components
import EmailField from '@components/Fields/EmailField/EmailField'
import PasswordField from '@components/Fields/PasswordField/PasswordField'
import BlankLayout from '@/layout/Blank'

// Custom css style
const useStyles = makeStyles(theme => ({
	layout: {
		marginTop: theme.spacing(15),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		width: '550px',
	},
	form: {
		width: '100%',
		marginTop: theme.spacing(2),
	},
	alert: {
		width: '100%',
		maxWidth: '26rem',
		marginTop: theme.spacing(6),
	},
	submit: {
		margin: theme.spacing(6, 0, 2),
	},
	link: {
		textDecoration: 'none',
	},
	divider: {
		width: '100%',
		margin: theme.spacing(4, 0, 4),
	},
}))

/**
 * Login view
 */
export default function Login() {
	const classes = useStyles()
	const history = useHistory()
	const appContext = useContext(AppContext)

	const [alertValue, setAlertValue] = useState('')
	const [isLoading, setLoading] = useState(false)

	const [emailInput, setEmailValue] = useState('')
	const [passwordInput, setPasswordValue] = useState('')

	function handleFormSubmit(event) {
		event.preventDefault()

		if (Boolean(emailInput) && Boolean(passwordInput)) {
			console.debug({ loginInput: emailInput, passwordInput: passwordInput })
			setLoading(true)

			// Login user with backend : Return a jwt token automaticlly saved by the web browser
			UserService.login(appContext, emailInput, passwordInput)
				.then(res => {
					setLoading(false)
					history.push('/home') // TODO : Email verification
				})
				.catch(err => {
					console.error(err)
					setAlertValue(
						'The username and password not found in our databases. Please check and try again.'
					)
					setLoading(false)
				})
		} else {
			setAlertValue(
				'One or more fields are invalid or incomplete. Please check it and try again.'
			)
		}
	}

	return (
		<BlankLayout loading={isLoading} className={classes.layout}>
			{Boolean(alertValue) && (
				<Alert
					variant="outlined"
					severity="error"
					onClose={() => {
						setAlertValue('')
					}}
					className={classes.alert}
				>
					{alertValue}
				</Alert>
			)}

			<form className={classes.form} noValidate onSubmit={handleFormSubmit}>
				{/* Email */}
				<EmailField
					id="email-input"
					label="Email"
					onChange={event => setEmailValue(event.target.value)}
				/>

				{/* Password */}
				<PasswordField
					id="password-field"
					label="Password"
					onChange={event => setPasswordValue(event.target.value)}
					visibleToggleable={true}
					autoComplete="new-password"
				/>

				<Button
					type="submit"
					fullWidth
					variant="contained"
					color="secondary"
					className={classes.submit}
				>
					Sign In
				</Button>
				<Link
					href="/forgot-password"
					variant="body1"
					color="secondary"
					className={classes.link}
				>
					Forgot password?
				</Link>
			</form>

			<Divider orientation="horizontal" className={classes.divider} />

			<Link
				href="/register"
				variant="body1"
				color="secondary"
				className={classes.link}
			>
				Don't have an account ? Sign Up
			</Link>
		</BlankLayout>
	)
}
