/* eslint-disable react-hooks/exhaustive-deps */
import { useCallback, useContext, useEffect, useRef, useState } from 'react'
import { useHistory } from 'react-router'
import { Box, makeStyles } from '@material-ui/core'

import MainLayout from '@/layout/Main'
import PostList from '@/components/Post/PostList'
import AppContext from '@/contexts/AppContext'
import { PostService } from '@/services/post'

// Custom css style
const useStyles = makeStyles(theme => ({
	parent: {
		height: 'inherit',
		overflowY: 'scroll',
		paddingTop: '10px',
		backgroundColor: theme.palette.background.paper,
	},
}))

export default function Subs() {
	const classes = useStyles()
	const history = useHistory()
	const observer = useRef()

	const appContext = useContext(AppContext)
	const user = appContext.value.user

	const nbItems = 25

	const [list, setList] = useState([])
	const [page, setPage] = useState(0)
	const [hasMore, setHasMore] = useState(true)
	const [isLoading, setIsLoading] = useState(false)

	useEffect(() => {
		fetchData()
	}, [user])

	/**
	 * Get follower post
	 */
	function fetchData() {
		if (user && hasMore) {
			setIsLoading(true)
			setPage(page + 1)
			PostService.getFollowerPost(user.id, page)
				.then(res => {
					setHasMore(res.length === nbItems)
					setList([...list, ...res])
					setIsLoading(false)
				})
				.catch(err => console.log(err))
		}
	}

	function reset() {
		setList([])
		setPage(0)
		setHasMore(true)
		setIsLoading(false)
	}

	function handleForward(postId) {
		reset()
		history.push(`/post/${postId}`)
	}

	/**
	 * Callback use to check if element is the last
	 */
	const lastItemRef = useCallback(
		node => {
			if (isLoading) return
			if (observer.current) observer.current.disconnect()

			observer.current = new IntersectionObserver(entries => {
				if (entries[0].isIntersecting) {
					fetchData()
				}
			})

			if (node) observer.current.observe(node)
		},
		[hasMore, isLoading]
	)

	return (
		<MainLayout loading={isLoading}>
			<Box className={classes.parent}>
				<PostList list={list} last={lastItemRef} forward={handleForward} />
			</Box>
		</MainLayout>
	)
}
