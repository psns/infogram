/* eslint-disable react-hooks/exhaustive-deps */

import { useContext, useEffect } from 'react'
import {
	BrowserRouter as Router,
	Route,
	Redirect,
	Switch,
} from 'react-router-dom'

// MUI Imports
import { createMuiTheme } from '@material-ui/core'
import { ThemeProvider } from '@material-ui/styles'
import CssBaseline from '@material-ui/core/CssBaseline'

// Infogram themes
import darkTheme from '@themes/index'

// Infogram contexts
import AppContext from '@/contexts/AppContext'

// Infogram services
import { UserService } from '@/services/user'

// Infogram components
import RouteGuard from '@components/RouteGuard/RouteGuard.js'

// Infogram views
import Login from '@views/Login/Login.js'
import Register from '@views/Register/Register.js'
import Subs from '@views/Subs/Subs.js'
import Profile from '@views/Profile/Profile.js'
import Post from '@views/Post/Post'
import NotFound from '@views/NotFound/NotFound'
import Popular from '@views/Popular/Popular'
import Tag from './Tag/Tag'
import Account from './Account/Account'

const theme = createMuiTheme(darkTheme)

function App() {
	const appContext = useContext(AppContext)

	useEffect(() => {
		// Refresh App Context if user token exist
		if (UserService.getJwtToken()) {
			UserService.store(appContext)
		}
	}, [])

	return (
		<ThemeProvider theme={theme}>
			<CssBaseline />
			<Router>
				<Switch>
					<Route exact path="/">
						<Redirect to="/home" />
					</Route>
					<Route exact path="/login" component={Login} />
					<Route exact path="/register" component={Register} />
					<RouteGuard
						exact
						path="/home"
						component={Popular}
						redirectTo="/login"
					/>
					<RouteGuard exact path="/subs" component={Subs} redirectTo="/login" />
					<RouteGuard
						exact
						path="/tag/:tagQuery"
						component={Tag}
						redirectTo="/login"
					/>
					<RouteGuard
						path="/profile/:userId?"
						component={Profile}
						redirectTo="/login"
					/>
					<RouteGuard path="/account" component={Account} redirectTo="/login" />
					<RouteGuard
						path="/post/:postId"
						component={Post}
						redirectTo="/login"
					/>
					<Route exact path="/*" component={NotFound} />
				</Switch>
			</Router>
		</ThemeProvider>
	)
}

export default App
