/* eslint-disable react-hooks/exhaustive-deps */
import { useCallback, useContext, useEffect, useRef, useState } from 'react'
import { useHistory, useParams } from 'react-router'

// Import Material UI
import { Box, Divider, makeStyles, Paper, Tab, Tabs } from '@material-ui/core'
import { Person, Star } from '@material-ui/icons'

// Import Infogram component
import ProfileHead from '@/components/Profile/ProfileHead'
import PostList from '@/components/Post/PostList'

// Infogram context
import AppContext from '@contexts/AppContext'

//Import Infogram  service
import { PostService } from '@/services/post'
import { UserService } from '@/services/user'

//Import Infogram Layout
import MainLayout from '@/layout/Main'

// Custom css style
const useStyles = makeStyles(theme => ({
	parent: {
		height: 'inherit',
		overflowY: 'scroll',
		paddingTop: '10px',
		backgroundColor: theme.palette.background.paper,
	},
	dividerParent: {
		margin: '1px',
	},
	box: {
		display: 'flex',
		margin: '30px 30px',
		'& > *': {
			width: '100%',
		},
	},
}))

export default function Profile() {
	const classes = useStyles()
	const history = useHistory()
	const { userId } = useParams()

	const nbItems = 25
	const appContext = useContext(AppContext)
	const user = appContext.value.user

	const [tab, setTab] = useState(0)
	const [list, setList] = useState([])
	const [page, setPage] = useState(0)
	const [hasMore, setHasMore] = useState(true)
	const [isLoading, setIsLoading] = useState(false)
	const [uid, setUid] = useState(null)

	const observer = useRef()

	useEffect(() => {
		if (userId) {
			setUid(userId)
		} else if (user) {
			setUid(user.id)
		}
	}, [user, userId])

	useEffect(() => {
		fetchData()
	}, [uid, tab])

	/**
	 * Get follower post
	 */
	function fetchData() {
		if (uid && hasMore) {
			setIsLoading(true)
			setPage(page + 1)
			if (tab === 0) {
				PostService.getUserPost(uid, page)
					.then(res => {
						setHasMore(res.length === nbItems)
						setList([...list, ...res])
						setIsLoading(false)
					})
					.catch(err => console.error(err))
			} else {
				UserService.getFavorites(uid, page)
					.then(res => {
						res = res.map(post => post.Post)
						setHasMore(res.length === nbItems)
						setList([...list, ...res])
						setIsLoading(false)
					})
					.catch(err => console.error(err))
			}
		}
	}

	/**
	 * handles the change of tab panel
	 *
	 * @param {event} e
	 * @param {int} tabId
	 */
	function handleTabChange(e, tabId) {
		if (user && uid === user.id) {
			reset()
			setTab(tabId)
		}
	}

	function reset() {
		setList([])
		setPage(0)
		setHasMore(true)
		setIsLoading(false)
	}

	function handleForward(postId) {
		reset()
		history.push(`/post/${postId}`)
	}

	/**
	 * Callback use to check if element is the last
	 */
	const lastItemRef = useCallback(
		node => {
			if (isLoading) return
			if (observer.current) observer.current.disconnect()

			observer.current = new IntersectionObserver(entries => {
				if (entries[0].isIntersecting) {
					fetchData()
				}
			})

			if (node) observer.current.observe(node)
		},
		[hasMore, isLoading]
	)

	return (
		<MainLayout loading={isLoading}>
			<Box className={classes.parent}>
				<ProfileHead userId={userId} />
				<Divider className={classes.dividerParent} />
				<Paper>
					<Tabs
						value={tab}
						onChange={handleTabChange}
						variant="fullWidth"
						indicatorColor="secondary"
						textColor="secondary"
						aria-label="icon label tabs example"
					>
						<Tab icon={<Person />} label="PROFILE" />
						{user && uid === user.id && (
							<Tab icon={<Star />} label="FAVORITES" />
						)}
					</Tabs>
				</Paper>
				<Box className={classes.box}>
					<PostList list={list} last={lastItemRef} forward={handleForward} />
				</Box>
			</Box>
		</MainLayout>
	)
}
