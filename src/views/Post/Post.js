/* eslint-disable react-hooks/exhaustive-deps */
import { useCallback, useContext, useEffect, useRef, useState } from 'react'
import { useHistory, useParams } from 'react-router'
import { Box, Button, Divider, makeStyles } from '@material-ui/core'

import AppContext from '@/contexts/AppContext'
import MainLayout from '@/layout/Main'
import PostList from '@/components/Post/PostList'
import PostDetail from '@/components/Post/PostDetail'
import { PostService } from '@/services/post'

// Custom css style
const useStyles = makeStyles(theme => ({
	parent: {
		height: 'inherit',
		overflowY: 'scroll',
		paddingTop: '10px',
		backgroundColor: theme.palette.background.paper,
	},
	box: {
		display: 'flex',
		margin: '-30px 30px',
		'& > *': {
			width: '100%',
		},
	},
	divider: {
		marginLeft: '10px',
		margin: '30px 0',
		width: '1px',
	},
	dividerParent: {
		margin: '-20px',
		marginInline: '30px',
	},
	list: {
		width: '100%',
	},
}))

export default function Post() {
	const classes = useStyles()
	const history = useHistory()

	const appContext = useContext(AppContext)
	const user = appContext.value.user

	const nbItems = 25

	const { postId } = useParams()

	const [list, setList] = useState([])
	const [post, setPost] = useState(null)
	const [parent, setParent] = useState(null)
	const [page, setPage] = useState(0)
	const [hasMore, setHasMore] = useState(true)
	const [isLoading, setIsLoading] = useState(false)

	const observer = useRef()

	useEffect(() => {
		if (user) {
			PostService.getPost(postId).then(res => {
				if (res) {
					setPost(res)
					fetchData()
					if (res.parentId) {
						PostService.getPost(res.parentId).then(res => setParent(res))
					} else {
						setParent(null)
					}
				} else history.push('/home')
			})
		}
		if (post && postId !== post.id) {
			setList([])
			setPost(null)
			setParent(null)
			setPage(0)
			setHasMore(true)
			setIsLoading(false)
		}
	}, [user, postId])

	/**
	 * Get follower post
	 */
	function fetchData() {
		if (user && hasMore) {
			setIsLoading(true)
			PostService.getResponsePost(postId, page).then(res => {
				setHasMore(res.length === nbItems)
				setList([...list, ...res])
				setIsLoading(false)
			})
			setPage(page + 1)
		}
	}

	/**
	 * Reset data when change
	 */
	function reset() {
		setList([])
		setPost(null)
		setParent(null)
		setPage(0)
		setHasMore(true)
		setIsLoading(false)
	}

	/**
	 * Handler for back button
	 */
	function handleBack() {
		reset()
		history.go(-1)
	}

	/**
	 * Handler for post
	 *
	 * @param {string} postId
	 */
	function handleForward(postId) {
		reset()
		history.push(`/post/${postId}`)
	}

	/**
	 * Callback use to check if element is the last
	 */
	const lastItemRef = useCallback(
		node => {
			if (isLoading) return
			if (observer.current) observer.current.disconnect()

			observer.current = new IntersectionObserver(entries => {
				if (entries[0].isIntersecting) {
					fetchData()
				}
			})

			if (node) observer.current.observe(node)
		},
		[hasMore, isLoading]
	)

	return (
		<MainLayout loading={isLoading}>
			{post && (
				<Box className={classes.parent}>
					<Button onClick={() => handleBack()}>Go back</Button>
					{parent && (
						<>
							<PostDetail post={parent} large forward={handleForward} />
							<Divider className={classes.dividerParent} />
						</>
					)}
					<PostDetail post={post} large />
					<Box className={classes.box}>
						<Divider
							orientation="vertical"
							flexItem
							className={classes.divider}
						/>
						{!isLoading && (
							<PostList
								list={list}
								last={lastItemRef}
								forward={handleForward}
							/>
						)}
					</Box>
				</Box>
			)}
		</MainLayout>
	)
}
