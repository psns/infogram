/* eslint-disable react-hooks/exhaustive-deps */
import { useCallback, useContext, useEffect, useRef, useState } from 'react'
import { useHistory, useParams } from 'react-router'
import { Box, Divider, makeStyles, Typography } from '@material-ui/core'

import MainLayout from '@/layout/Main'
import DateSelect from '@/components/Date/DateSelect'
import PostList from '@/components/Post/PostList'
import AppContext from '@/contexts/AppContext'
import { TagService } from '@/services/tag'

// Custom css style
const useStyles = makeStyles(theme => ({
	parent: {
		height: 'inherit',
		overflowY: 'scroll',
		paddingTop: '10px',
		backgroundColor: theme.palette.background.paper,
	},
	date: {
		backgroundColor: theme.palette.background.paper,
		paddingTop: '10px',
		alignItems: 'center',
	},
	divider: {
		padding: '1px',
		width: '95%',
		margin: 'auto',
	},
	tag: {
		textAlign: 'center',
		marginBlock: '10px',
	},
}))

export default function Tag() {
	const classes = useStyles()
	const history = useHistory()
	const observer = useRef()
	const { tagQuery } = useParams()

	const appContext = useContext(AppContext)
	const user = appContext.value.user

	const nbItems = 25
	const HOUR = 1000 * 60 * 60

	const [list, setList] = useState([])
	const [page, setPage] = useState(0)
	const [hasMore, setHasMore] = useState(true)
	const [isLoading, setIsLoading] = useState(false)
	const [tag, setTag] = useState('')
	const [day, setDay] = useState(1)

	useEffect(() => {
		fetchData()
	}, [user, tag, day])

	useEffect(() => {
		reset()
		setTag(tagQuery)
	}, [tagQuery])

	/**
	 * Get follower post
	 */
	function fetchData() {
		if (user && hasMore && tag) {
			setIsLoading(true)
			setPage(page + 1)
			const until = new Date(
				Date.now() - (day > 0 ? day * 24 * HOUR : Date.now())
			).toISOString()
			TagService.getPostTag(page, tag, until)
				.then(res => {
					setHasMore(res.length === nbItems)
					setList([...list, ...res])
					setIsLoading(false)
				})
				.catch(err => console.log(err))
		}
	}

	function reset() {
		setList([])
		setPage(0)
		setHasMore(true)
		setIsLoading(false)
	}

	function handleForward(postId) {
		reset()
		history.push(`/post/${postId}`)
	}

	function handlerDay(day) {
		reset()
		setDay(day)
	}

	/**
	 * Callback use to check if element is the last
	 */
	const lastItemRef = useCallback(
		node => {
			if (isLoading) return
			if (observer.current) observer.current.disconnect()

			observer.current = new IntersectionObserver(entries => {
				if (entries[0].isIntersecting) {
					fetchData()
				}
			})

			if (node) observer.current.observe(node)
		},
		[hasMore, isLoading]
	)

	return (
		<MainLayout loading={isLoading}>
			<Box className={classes.date}>
				<Typography className={classes.tag} variant="h4">
					#{decodeURIComponent(tag)}
				</Typography>
				<DateSelect day={day} setDay={handlerDay} />
				<Divider className={classes.divider} />
			</Box>
			<Box className={classes.parent}>
				{!isLoading && (
					<PostList list={list} last={lastItemRef} forward={handleForward} />
				)}
			</Box>
		</MainLayout>
	)
}
