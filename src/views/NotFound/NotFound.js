import { Link } from 'react-router-dom'
import { Container, makeStyles } from '@material-ui/core'

// Custom css style
const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1,
	},
}))

/**
 * NotFound view
 */
export default function NotFound() {
	const classes = useStyles()

	return (
		<Container fixed className={classes.root}>
			<h1>404</h1>
			<h2>Page Not Found</h2>
			<h6>
				We're sorry, the page requested could not be found. Please go back to
				the home page.
			</h6>
			<Link to="/home">&#60; GO HOME</Link>
		</Container>
	)
}
