import { useState } from 'react'
import { useHistory } from 'react-router'

import {
	Avatar,
	Box,
	Dialog,
	DialogContent,
	makeStyles,
	TextField,
	Typography,
} from '@material-ui/core'
import { Check } from '@material-ui/icons'

import TextAreaField from '@components/Fields/TextAreaField/TextAreaField'
import PanelLayout from '@/layout/Panel'

import { UserService } from '@/services/user'
import { FilerService } from '@/services/filer'

const useStyles = makeStyles(theme => ({
	paper: {
		backgroundColor: theme.palette.background.paper,
		border: '2px solid #000',
		width: '60vh',
		height: '60vh',
		overflowY: 'hidden',
	},
	dialog: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		'& > * > *': {
			maxWidth: 'inherit',
			overflow: 'hidden',
		},
	},
	panel: {
		height: '100%',
		alignItems: 'center',
	},
	image: {
		alignSelf: 'center',
		width: '100px',
		height: 'auto',
	},
	avatar: {
		width: '100px',
		height: '100px',
	},
	content: {
		height: '100%',
		'& > *': {
			display: 'flex',
			alignItems: 'center',
			justifyContent: 'space-between',
		},
	},
	separator: {
		height: '100px',
		alignItems: 'center',
		display: 'flex',
		justifyContent: 'center',
	},
	description: {
		marginTop: '10px',
		flexDirection: 'column',
		alignItems: 'baseline',
	},
	typo: {
		width: '150px',
	},
	box: {
		marginBlock: '10px',
	},
}))

export default function EditProfile({ open, setOpen, profile, user }) {
	const classes = useStyles()
	const history = useHistory()

	const [error, setError] = useState('')

	const [name, setName] = useState(undefined)
	const [description, setDescription] = useState(undefined)
	const [profileImage, setProfileImage] = useState(null)
	const [background, setBackground] = useState(null)

	function handleClose() {
		setOpen(false)
	}

	async function handleSubmit() {
		UserService.updateUser(user.id, { name: name }).then(res => {
			if (res.code) setError('You get rick rolled')
		})
		UserService.updateProfile(user.id, description).then(res => {
			if (res.code) setError('You get rick rolled')
		})
		console.log(name, description, profileImage, background)

		profileImage &&
			(await FilerService.upload(profileImage, user.id, 'profile').then(res => {
				if (res.code) setError('You get rick rolled')
			}))
		background &&
			(await FilerService.upload(background, user.id, 'background').then(
				res => {
					if (res.code) setError('You get rick rolled')
				}
			))

		history.go()
	}

	return (
		<Dialog
			open={open}
			onClose={() => handleClose()}
			aria-labelledby="edit-profile"
			className={classes.dialog}
		>
			<DialogContent className={classes.paper}>
				<PanelLayout
					title={'Edit profile'}
					submitIcon={<Check color="secondary" />}
					submitTooltip="APPLY"
					onReturn={handleClose}
					onSubmit={handleSubmit}
					className={classes.panel}
				>
					<Box className={classes.content}>
						<Box className={classes.box}>
							<Typography className={classes.typo}>Background</Typography>
							<img
								className={classes.image}
								src={`/api/filer/download/${encodeURIComponent(
									profile?.backgroundImage ?? 'default/profile/default.png'
								)}`}
								alt="background"
							/>
							<input
								type="file"
								onChange={e => setBackground(e.target.files[0])}
							/>
						</Box>
						<Box className={classes.box}>
							<Typography className={classes.typo}>Profile</Typography>
							<Avatar
								className={classes.avatar}
								src={`/api/filer/download/${encodeURIComponent(
									profile?.image ?? 'default/profile/default.png'
								)}`}
							/>
							<input
								type="file"
								maxLength="20"
								onChange={e => setProfileImage(e.target.files[0])}
							/>
						</Box>
					</Box>
					<Box>
						{error && (
							<Typography color="error" variant="h5">
								{error}
							</Typography>
						)}
					</Box>
					<Box className={classes.content}>
						<Box>
							<Typography className={classes.typo}>Name</Typography>
							<TextField
								onChange={e => setName(e.target.value)}
								defaultValue={user.name}
							/>
						</Box>
						<Box className={classes.description}>
							<Typography className={classes.typo}>Description</Typography>
							<TextAreaField
								defaultValue={profile?.description}
								onChange={e => setDescription(e.target.value)}
							/>
						</Box>
					</Box>
				</PanelLayout>
			</DialogContent>
		</Dialog>
	)
}
