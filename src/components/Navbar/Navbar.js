// MUI Imports
import { AppBar, Toolbar } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

// Infogram custom components
import IGHomeIcon from '@/components/Navbar/IGHomeIcon/IGHomeIcon'
import IGSearchBar from '@/components/Navbar/IGSearchBar/IGSearchBar'
import IGNavMenu from '@/components/Navbar/IGNavMenu/IGNavMenu'

// Custom css style
const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1,
		padding: 0,
		maxWidth: '100%',
	},
}))

/**
 * Navbar component
 *
 * @returns JSX Elements
 */
export default function Navbar({ onPostClick, onNotifClick, onMessageClick }) {
	const classes = useStyles()

	return (
		<AppBar position="fixed">
			<Toolbar>
				<IGHomeIcon />
				<div className={classes.root} />
				<IGSearchBar />
				<div className={classes.root} />
				<IGNavMenu
					onPostClick={onPostClick}
					onNotifClick={onNotifClick}
					onMessageClick={onMessageClick}
				/>
			</Toolbar>
		</AppBar>
	)
}
