import { useHistory } from 'react-router-dom'

// MUI core Imports
import { makeStyles } from '@material-ui/core/styles'
import { Avatar, IconButton, Typography } from '@material-ui/core'

// Custom css style
const useStyles = makeStyles(theme => ({
	navIcon: {
		marginRight: '0.75em',
	},
	navTitle: {
		fontWeight: '500',
	},
	menuButton: {
		borderRadius: 2,
		paddingTop: 0,
		paddingBottom: 0,
	},
}))

/**
 * IGHomeIcon component is the title and logo of navbar
 *
 * @returns JSX Elements
 */
export default function IGHomeIcon() {
	const classes = useStyles()
	const history = useHistory()

	return (
		<div>
			<IconButton
				edge="start"
				className={classes.menuButton}
				onClick={() => history.push('/home')}
			>
				<Avatar
					variant="square"
					src="/infogram.png"
					className={classes.navIcon}
				/>
				<Typography variant="h5" noWrap className={classes.navTitle}>
					&#60;Infogram &#47;&#62;
				</Typography>
			</IconButton>
		</div>
	)
}
