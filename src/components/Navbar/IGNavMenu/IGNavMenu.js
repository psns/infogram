import React, { useContext } from 'react'
import { useHistory } from 'react-router'

// MUI core Imports
import {
	IconButton,
	Badge,
	Box,
	Divider,
	CircularProgress,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

// MUI Icons
import MailIcon from '@material-ui/icons/Mail'
import NotificationsIcon from '@material-ui/icons/Notifications'
import AddBoxIcon from '@material-ui/icons/AddBox'

// Infogram App Context
import AppContext from '@/contexts/AppContext'

// Infogram Services
import { UserService } from '@/services/user'

// Infogram custom components
import DropDownButton from '@/components/Buttons/DropDownButton/DropDownButton'

// Custom css style
const useStyles = makeStyles(theme => ({
	navMenu: {
		display: 'none',
		[theme.breakpoints.up('md')]: {
			display: 'flex',
		},
	},
	navMenuButton: {
		borderRadius: 2,
		paddingTop: 0,
		paddingBottom: 0,
	},
	navMenuIcon: {
		marginTop: 0,
		marginBottom: 0,
		paddingTop: 0,
		paddingBottom: 0,
	},
	navMenuDivider: {
		marginLeft: '0.7em',
	},
	navMenuDivider2: {
		margin: '10px 1em 10px 1em',
	},
	registerBtn: {
		border: '1px solid',
		borderRadius: 5,
	},
	spinner: {
		marginTop: '3px',
	},
}))

/**
 * IGNavMenu component is the menu items
 *
 * @returns JSX Elements
 */
export default function IGNavMenu({
	onPostClick,
	onNotifClick,
	onMessageClick,
}) {
	const classes = useStyles()
	const history = useHistory()

	const appContext = useContext(AppContext)
	const User = UserService.getUser(appContext)

	// Drop down menu content
	const dropDownMenuItems = [
		{
			name: 'Profile',
			onClick: () => {
				history.push('/profile')
				history.go()
			},
		},
		{
			name: 'My Account',
			onClick: () => history.push('/account'),
		},
		{
			name: 'Logout',
			onClick: () => {
				UserService.logout(appContext)
				history.push('/')
			},
		},
	]

	// If user exist (logged), display menu buttons, else display login/register button
	if (User) {
		return (
			<div className={classes.navMenu}>
				<IconButton
					aria-label="create new post"
					color="secondary"
					className={classes.navMenuButton}
					onClick={onPostClick}
				>
					<AddBoxIcon className={classes.navMenuIcon} />
				</IconButton>

				<IconButton
					aria-label="show 4 new mails"
					className={classes.navMenuButton}
					onClick={onNotifClick}
				>
					<Badge badgeContent={4} color="secondary">
						<MailIcon />
					</Badge>
				</IconButton>
				<Box mr={1} />
				<IconButton
					aria-label="show 17 new notifications"
					className={classes.navMenuButton}
					onClick={onMessageClick}
				>
					<Badge badgeContent={17} color="secondary">
						<NotificationsIcon />
					</Badge>
				</IconButton>

				<Divider
					orientation="vertical"
					flexItem
					className={classes.navMenuDivider2}
				/>

				<DropDownButton
					ariaLabel="Account of current user"
					className={classes.navMenuButton}
					menuItems={dropDownMenuItems}
				>
					@{User.nickname}
				</DropDownButton>
			</div>
		)
	} else {
		return (
			<CircularProgress
				className={classes.spinner}
				size={30}
				thickness={4}
				color="secondary"
			/>
		)
	}
}
