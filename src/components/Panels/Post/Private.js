// MUI Imports
import {
	MenuItem,
	Select,
	makeStyles,
	FormControl,
	InputLabel,
	Grid,
} from '@material-ui/core'

// Infogram components
import DescriptionField from '@/components/Fields/DescriptionField/DescriptionField'
import TipsDialog from '@/components/TipsDialog/TipsDialog'

// Custom css style
const useStyles = makeStyles(theme => ({
	grid: {
		width: '100%',
		justifyContent: 'center',
		marginBottom: '1.5rem',
	},
	selector: {
		width: '100%',
	},
}))

export default function PrivateForm({
	type,
	description,
	visibility,
	data,
	onDescChange,
	onVisibilityChange,
	onFileSelect,
	onTypeChange,
}) {
	const classes = useStyles()

	return (
		<>
			<Grid
				container
				justify="space-between"
				alignItems="flex-end"
				className={classes.grid}
			>
				<Grid item xs={11}>
					<FormControl className={classes.selector}>
						<InputLabel id="visibility-selector-label">Visibility</InputLabel>
						<Select
							labelId="visibility-selector-label"
							id="visibility-selector"
							value={visibility}
							onChange={onVisibilityChange}
						>
							<MenuItem value={0} disabled></MenuItem>
							<MenuItem value={1}>Followers</MenuItem>
							<MenuItem value={2}>Protected</MenuItem>
						</Select>
					</FormControl>
				</Grid>
				<Grid item xs={1}>
					<TipsDialog title="Visibility Helper">
						- "Followers" authorize followers to see, comment, like/dislike and
						share your post.
						<br />- "Protected" authorize people following each other to see,
						comment, like/dislike and share your post.
					</TipsDialog>
				</Grid>
			</Grid>

			<DescriptionField
				type={type}
				description={description}
				data={data}
				onDescChange={onDescChange}
				onFileSelect={onFileSelect}
				onTypeChange={onTypeChange}
			/>
		</>
	)
}
