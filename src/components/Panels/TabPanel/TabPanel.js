import { Box } from '@material-ui/core'

export default function TabPanel({ children, value, index, ...props }) {
	return (
		<div id={`tabpanel-${index}`} hidden={value !== index} {...props}>
			<Box p={3}>{children}</Box>
		</div>
	)
}
