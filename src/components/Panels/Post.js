import { useState } from 'react'
import { useHistory } from 'react-router'

// MUI imports
import {
	Box,
	Chip,
	Divider,
	makeStyles,
	Paper,
	Tab,
	Tabs,
} from '@material-ui/core'
import { Group, Public, Send, VpnLock } from '@material-ui/icons'

// Infogram layouts
import PanelLayout from '@/layout/Panel'

// Infogram services
import { PostService } from '@/services/post'

// Inforgram components
import TabPanel from './TabPanel/TabPanel'
import PrivateForm from './Post/Private'
import PublicForm from './Post/Public'

// Custom css style
const useStyles = makeStyles(theme => ({
	root: {
		height: '100%',
	},
	paper: {
		padding: '5px',
		height: '100%',
		display: 'flex',
		flexDirection: 'column',
	},
	center: {
		textAlign: 'center',
	},
	chipbox: {
		marginTop: '1rem',
		marginBottom: '0.8rem',
	},
	chip: {
		marginInline: '0.2rem',
	},
}))

/**
 *  Post panel is used when an ser want to create a new post
 */
export default function PostPanel({ onReturn, parentId = null }) {
	const classes = useStyles()
	const history = useHistory()

	const [tab, setTab] = useState(0)

	const [postType, setPostType] = useState('TEXT')
	const [postTags, setPostTags] = useState([])
	const [postVisibility, setPostVisibility] = useState(0)
	const [postDescription, setPostDescription] = useState('')
	const [postData, setPostData] = useState(null)
	const [postGroupId, setPostGroupId] = useState(null)

	function handlePostDescriptionChange(event) {
		let tags = postTags
		const content = event.target.value
		setPostDescription(content)

		// Check tags in content and add them in post tags array
		if (content.includes('#') && content[content.length - 1] === ' ') {
			content.split(' ').forEach(word => {
				if (word[0] === '#' && !tags.includes(word.slice(1))) {
					tags = [...tags, word.slice(1)]
				}
			})
		}

		// Remove post tags from array if they're not exist anymore in content
		tags = tags.filter(tag => {
			let isIncluded = false
			content.split(' ').forEach(word => {
				if (word === `#${tag}`) {
					isIncluded = true
				}
			})
			return isIncluded
		})

		setPostTags(tags)
	}

	function handlePanelReturn(event) {
		resetPostStates()
		onReturn(event)
	}

	function handlePanelSubmit() {
		PostService.newPost({
			type: postType,
			tags: postTags,
			visibility: postVisibility,
			description: postDescription,
			data: postData,
			groupId: postGroupId,
			parentId: parentId,
		}).then(() => {
			history.push(history.location.pathname)
			history.go()
		})
		resetPostStates({ visibility: postVisibility })
	}

	function handleTabChange(e, tabId) {
		setTab(tabId)
		switch (tabId) {
			case 0:
				resetPostStates()
				break
			case 1:
				resetPostStates({ visibility: 1 })
				break
			case 2:
				resetPostStates({ visibility: 3 })
				break
			default:
				break
		}
	}

	function resetPostStates(data = {}) {
		setPostType(data.type || 'TEXT')
		setPostTags(data.tags || [])
		setPostVisibility(data.visibility || 0)
		setPostDescription(data.description || '')
		setPostData(data.data || null)
		setPostGroupId(data.groupId || null)
	}

	return (
		<PanelLayout
			title={parentId ? 'New Comment' : 'New Post'}
			className={classes.root}
			submitIcon={<Send color="secondary" />}
			submitTooltip="POST"
			onReturn={handlePanelReturn}
			onSubmit={handlePanelSubmit}
		>
			<Paper className={classes.paper} elevation={1}>
				<Tabs
					value={tab}
					onChange={handleTabChange}
					variant="fullWidth"
					indicatorColor="secondary"
					textColor="secondary"
					aria-label="icon label tabs example"
				>
					<Tab icon={<Public />} label="PUBLIC" />
					<Tab icon={<VpnLock />} label="PRIVATE" />
					<Tab icon={<Group />} label="GROUP" disabled />
				</Tabs>
				<Divider />

				<TabPanel index={0} value={tab}>
					<PublicForm
						type={postType}
						description={postDescription}
						data={postData}
						onDescChange={handlePostDescriptionChange}
						onFileSelect={data => setPostData(data)}
						onTypeChange={newType => setPostType(newType)}
					/>
				</TabPanel>

				<TabPanel index={1} value={tab}>
					<PrivateForm
						type={postType}
						description={postDescription}
						visibility={postVisibility}
						data={postData}
						onDescChange={handlePostDescriptionChange}
						onVisibilityChange={event => setPostVisibility(event.target.value)}
						onFileSelect={data => setPostData(data)}
						onTypeChange={newType => setPostType(newType)}
					/>
				</TabPanel>

				<TabPanel index={2} value={tab}>
					GROUP
				</TabPanel>

				{postTags.length > 0 && <Divider orientation="horizontal" />}
				<Box className={classes.chipbox}>
					{postTags.map((tag, idx) => {
						return (
							<Chip
								key={idx}
								variant="outlined"
								label={`#${tag}`}
								className={classes.chip}
							/>
						)
					})}
				</Box>
			</Paper>
		</PanelLayout>
	)
}
