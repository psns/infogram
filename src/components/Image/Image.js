import { useState } from 'react'

// MUI imports
import {
	Box,
	makeStyles,
	Dialog,
	DialogTitle,
	DialogContent,
} from '@material-ui/core'
import clsx from 'clsx'
import { NumberService } from '@/services/number'

// Custom css style
const useStyles = makeStyles(theme => ({
	root: {
		backgroundColor: theme.palette.common.black,
		marginTop: '0.5rem',
		marginBottom: '1rem',
		'&:hover': {
			transition: '0.2s',
			cursor: 'pointer',
			opacity: 0.7,
		},
	},
	dialog: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		'& > * > *': {
			maxWidth: 'inherit',
		},
	},
	image: {
		display: 'block',
		opacity: 1,
		transition: '.5s ease',
		backfaceVisibility: 'hidden',
		marginLeft: 'auto',
		marginRight: 'auto',
		maxHeight: '500px',
		maxWidth: '100%',
	},
	imageDialog: {
		maxHeight: '720px',
		maxWidth: '1280px',
		marginBottom: '1rem',
	},
}))

export default function Image({
	src,
	createdAt,
	alt = '',
	publisherId,
	postId,
}) {
	const classes = useStyles()

	const { name, extension, mimetype, size } = src
	const [isExifModalOpen, setExifModalOpen] = useState(false)

	return (
		<>
			<Box className={classes.root}>
				<img
					src={`/api/filer/download/${encodeURIComponent(
						`${publisherId}/post/${postId}.${extension}`
					)}`}
					onClick={() => setExifModalOpen(true)}
					alt={alt}
					className={classes.image}
				/>
			</Box>
			<Dialog
				open={isExifModalOpen}
				onClose={() => setExifModalOpen(false)}
				aria-labelledby="exif-dialog-title"
				aria-describedby="exif-dialog-description"
				className={classes.dialog}
			>
				<DialogTitle id="exif-dialog-title">{name}</DialogTitle>
				<DialogContent>
					<img
						src={`/api/filer/download/${encodeURIComponent(
							`${publisherId}/post/${postId}.${extension}`
						)}`}
						alt={alt}
						className={clsx(classes.image, classes.imageDialog)}
					/>
					<hr />
					<br />
					EXIF:
					<ul>
						<li>File Name: {name}</li>
						<li>File Size: {NumberService.formatBytes(size, 3)}</li>
						<li>File Modify Date: {new Date(createdAt).toLocaleString()}</li>
						<li>File Type: {extension}</li>
						<li>MIME Type: {mimetype}</li>
					</ul>
				</DialogContent>
			</Dialog>
		</>
	)
}
