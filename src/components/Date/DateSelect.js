import { Box, Button, ButtonGroup, makeStyles } from '@material-ui/core'

// Custom css style
const useStyles = makeStyles(theme => ({
	group: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		marginBottom: '10px',
	},
	button: {
		fontSize: '0.8rem',
		fontWeight: 'bold',
	},
}))

export default function DateSelect({ day, setDay }) {
	const classes = useStyles()
	const buttons = [
		{
			text: '1 Day',
			value: 1,
		},
		{
			text: '1 Week',
			value: 7,
		},
		{
			text: '1 Month',
			value: 30,
		},
		{
			text: '1 Year',
			value: 365,
		},
		{
			text: 'All time',
			value: -1,
		},
	]

	return (
		<Box className={classes.group}>
			<ButtonGroup>
				{buttons.map(button => (
					<Button
						key={button.value}
						color="secondary"
						className={classes.button}
						variant={day === button.value ? 'contained' : ''}
						onClick={() => button.value !== day && setDay(button.value)}
					>
						{button.text}
					</Button>
				))}
			</ButtonGroup>
		</Box>
	)
}
