import { useContext, useEffect, useState } from 'react'
import { useHistory } from 'react-router'

// MUI Imports
import {
	Box,
	Button,
	Grid,
	IconButton,
	makeStyles,
	Menu,
	MenuItem,
	Snackbar,
	Typography,
} from '@material-ui/core'
import { Comment, Favorite, MoreHoriz, Share } from '@material-ui/icons'
import { Alert } from '@material-ui/lab'

// Infogram services
import { NumberService } from '@/services/number'
import { PostService } from '@/services/post'
import { UserService } from '@/services/user'

import AppContext from '@/contexts/AppContext'

// Infogram components
import PostScore from './PostScore'

// Custom css style
const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1,
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingRight: '10px',
	},
	comms: {
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		'& > span > p': {
			fontSize: '0.8rem',
		},
	},
	number: {
		fontWeight: 'bold',
		width: '40px',
	},
	icon: {
		width: theme.spacing(2),
		height: theme.spacing(2),
	},
	selected: {
		color: theme.palette.primary.dark,
	},
}))

export default function PostFooter({ post }) {
	const classes = useStyles()
	const history = useHistory()
	const appContext = useContext(AppContext)

	const user = appContext.value.user
	const { id, publisherId } = post

	const [comments, setComments] = useState(0)
	const [favorite, setFavorite] = useState(false)
	const [open, setOpen] = useState(false)
	const [anchorEl, setAnchorEl] = useState(null)

	const [text, setText] = useState('')

	useEffect(() => {
		PostService.getNbResponse(id).then(res => setComments(res.count))
		if (user)
			UserService.getSpecificFavorite(user.id, id).then(res => setFavorite(res))
	}, [id, user])

	function handleNewComment(event) {
		history.push(`/post/${id}?new-comment=${id}`)
		history.go()
	}

	function handleShare() {
		setText('Copied to clipboard !')
		navigator.clipboard.writeText(`${window.location.origin}/post/${id}`)
		setOpen(true)
	}

	/**
	 * Handler for favorite button
	 */
	function handleFavorite() {
		if (favorite) {
			UserService.removeFavorite(user.id, id).then(res => setFavorite(null))
		} else {
			setText('Add to favorite !')
			UserService.addFavorite(user.id, id).then(res => setFavorite(res))
			setOpen(true)
		}
	}

	/**
	 * Handler for delete button
	 */
	function handleDelete() {
		PostService.deletePost(id).then(res => console.log(res))
		history.go()
	}

	/**
	 * Handler for close button
	 */
	function handleClose(event, reason) {
		if (reason === 'clickaway') {
			return
		}
		setOpen(false)
	}

	function handleClick(event) {
		setAnchorEl(event.currentTarget)
	}

	function handleCloseMenu() {
		setAnchorEl(null)
	}

	return (
		<>
			<Grid container className={classes.root}>
				<PostScore postId={id} />
				<Button onClick={handleNewComment} className={classes.comms}>
					<Comment className={classes.icon} />
					<Typography className={classes.number}>
						{NumberService.format(comments, 1)}
					</Typography>
					<Typography>Comment{comments > 1 && 's'}</Typography>
				</Button>
				<Box>
					<IconButton onClick={() => handleFavorite()}>
						<Favorite
							className={classes.icon && favorite ? classes.selected : ''}
						/>
					</IconButton>
					<IconButton onClick={() => handleShare()}>
						<Share className={classes.icon} />
					</IconButton>

					<IconButton onClick={e => handleClick(e)}>
						<MoreHoriz className={classes.icon} />
					</IconButton>
				</Box>
			</Grid>
			<Snackbar open={open} autoHideDuration={1500} onClose={handleClose}>
				<Alert onClose={handleClose} severity="info">
					{text}
				</Alert>
			</Snackbar>
			<Menu
				id="detail-post"
				keepMounted
				open={Boolean(anchorEl)}
				anchorEl={anchorEl}
				onClose={handleCloseMenu}
			>
				<MenuItem onClick={() => history.push(`/post/${id}`)}>Details</MenuItem>
				{user.id === publisherId && (
					<MenuItem onClick={() => handleDelete()}>Delete</MenuItem>
				)}
			</Menu>
		</>
	)
}
