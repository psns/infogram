// MUI imports
import {
	Box,
	Card,
	CardContent,
	Divider,
	Grid,
	makeStyles,
	Typography,
} from '@material-ui/core'

// Infogram components
import BadgeAvatar from './Avatar/BadgeAvatar'
import UserAvatar from './Avatar/UserAvatar'
import PostFooter from './PostFooter'
import Image from '../Image/Image'
import PostTags from './PostTags'
import clsx from 'clsx'

// Custom css style
const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1,
		marginInline: '-10px',
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	detail: {
		margin: '30px',
		backgroundColor: theme.palette.background.default,
		display: 'flex',
		width: '70vh',
		'&:hover': {
			backgroundColor: theme.palette.background.paperHover,
			transition: '0.4s',
			cursor: 'pointer',
		},
	},
	large: {
		width: 'auto',
	},
	post: {
		width: '100%',
	},
	title: {
		textAlign: 'center',
	},
	text: {
		paddingBottom: '10px',
		marginInline: '10px',
	},
	user: {
		display: 'flex',
		alignItems: 'center',
	},
	date: {
		color: theme.palette.grey[900],
		fontSize: '0.7rem',
	},
}))

export default function PostDetail({ post, large, forward, reference }) {
	const classes = useStyles()

	const { id, description, content, publisherId, createdAt, type, Tags } = post
	const date = new Date(createdAt)

	return (
		<Card
			className={large ? clsx(classes.detail, classes.large) : classes.detail}
			ref={reference}
			onDoubleClick={() => forward && forward(id)}
		>
			<Box className={classes.post}>
				<Grid container className={classes.root}>
					<Box className={classes.user}>
						<UserAvatar userId={publisherId} />
						<Typography className={classes.date}>
							{date.toLocaleString()}
						</Typography>
					</Box>
					<Box>
						<BadgeAvatar postId={id} />
					</Box>
				</Grid>
				{description && <CardContent>{description}</CardContent>}
				{type === 'MEDIA' && (
					<Image
						src={content}
						createdAt={createdAt}
						publisherId={publisherId}
						postId={id}
					/>
				)}
				<PostTags tags={Tags} />
				<Divider />
				<PostFooter post={post} />
			</Box>
		</Card>
	)
}
