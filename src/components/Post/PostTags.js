import { useHistory } from 'react-router'
import { Box, Chip, makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
	tagsBox: {
		paddingBlock: '1rem',
		display: 'inline-flex',
		flexWrap: 'wrap',
		justifyContent: 'flex-start',
		marginInline: '10px',
	},
	tag: {
		margin: '0.1rem',
	},
}))

export default function PostTags({ tags }) {
	const classes = useStyles()
	const history = useHistory()

	/**
	 * Handler for tag redirect
	 *
	 * @param {string} tag
	 */
	function handleTag(tag) {
		history.push(`/tag/${tag}`)
	}

	return (
		<Box className={classes.tagsBox}>
			{tags.map(tag => (
				<Chip
					key={tag.name}
					variant="outlined"
					label={`#${tag.name}`}
					className={classes.tag}
					size="small"
					onClick={() => handleTag(tag.name)}
				/>
			))}
		</Box>
	)
}
