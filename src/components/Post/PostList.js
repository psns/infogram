// MUI Imports
import { Box, makeStyles, Typography } from '@material-ui/core'

// Infogram components
import PostDetail from './PostDetail'

// Custom css style
const useStyles = makeStyles(theme => ({
	postContener: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		margin: '0 20px',
		backgroundColor: theme.palette.background.paper,
	},
	empty: {
		position: 'relative',
		top: '50%',
		margin: 'auto',
		textAlign: 'center',
		alignItems: 'center',
	},
}))

export default function PostList({ list, last, forward }) {
	const classes = useStyles()

	return (
		<Box className={classes.postContener}>
			{list.length === 0 ? (
				<Box className={classes.empty}>
					<Typography variant="h5"> Is empty here</Typography>
				</Box>
			) : (
				list.map((post, index) =>
					index + 1 === list.length ? (
						<PostDetail
							key={index}
							post={post}
							forward={forward}
							reference={last}
						/>
					) : (
						<PostDetail key={index} post={post} forward={forward} />
					)
				)
			)}
		</Box>
	)
}
