/* eslint-disable react-hooks/exhaustive-deps */
import { useContext, useEffect, useState } from 'react'
import { Box, IconButton, makeStyles, Typography } from '@material-ui/core'
import { ThumbDown, ThumbUp } from '@material-ui/icons'

// Infogram app context
import AppContext from '@/contexts/AppContext'

// Infogram services
import { PostService } from '@/services/post'
import { NumberService } from '@/services/number'

// Custom css style
const useStyles = makeStyles(theme => ({
	score: {
		display: 'flex',
		flexDirection: 'row',
		width: '20px',
		height: 'inherit',
		alignItems: 'center',
		marginRight: '10px',
	},
	value: {
		marginInline: '5px',
		textAlign: 'center',
		fontSize: '0.8rem',
		minWidth: '40px',
		maxWidth: '40px',
	},
	icon: {
		width: theme.spacing(2),
		height: theme.spacing(2),
	},
	selected: {
		color: theme.palette.primary.dark,
	},
}))

export default function PostScore({ postId }) {
	const classes = useStyles()
	const [score, setScore] = useState(0)
	const [userScore, setUserScore] = useState(0)

	const appContext = useContext(AppContext)
	const user = appContext.value.user
	useEffect(() => {
		fetchTotal()
		PostService.getUserScore(user?.id, postId).then(res =>
			setUserScore(res?.value ?? 0)
		)
	}, [postId, user])

	/**
	 * Get score of the post
	 */
	function fetchTotal() {
		PostService.getScore(postId).then(res => {
			const value = res?.sum?.value ?? 0
			setScore(value)
		})
	}

	/**
	 * Handler for score buttons
	 *
	 * @param {number} value
	 */
	function handleButton(value) {
		PostService.assignScore(
			user.id,
			postId,
			value === userScore ? 0 : value
		).then(res => {
			setUserScore(res.value)
			fetchTotal()
		})
	}

	return (
		<Box className={classes.score}>
			<IconButton onClick={() => handleButton(1)}>
				<ThumbUp
					className={classes.icon && userScore === 1 ? classes.selected : ''}
				/>
			</IconButton>
			<Typography className={classes.value}>
				{NumberService.format(score, 2)}
			</Typography>
			<IconButton onClick={() => handleButton(-1)}>
				<ThumbDown
					className={classes.icon && userScore === -1 ? classes.selected : ''}
				/>
			</IconButton>
		</Box>
	)
}
