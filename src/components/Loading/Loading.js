import { Backdrop, CircularProgress, makeStyles } from '@material-ui/core'

// Custom css style
const useStyles = makeStyles(theme => ({
	backdrop: {
		zIndex: theme.zIndex.drawer + 1,
		color: '#fff',
	},
}))

export default function Loading({ active }) {
	const classes = useStyles()

	return (
		<Backdrop className={classes.backdrop} open={active}>
			<CircularProgress color="inherit" />
		</Backdrop>
	)
}
