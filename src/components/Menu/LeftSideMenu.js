import { useEffect, useState } from 'react'

// MUI Imports
import { Paper, Divider, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

// Infogram services
import { TagService } from '@/services/tag'

// Infogram components
import MenuList from './MenuList/MenuList'
import MenuFooter from './MenuFooter/MenuFooter'
import MenuTags from './MenuTags/MenuTags'

const useStyles = makeStyles(theme => ({
	root: {
		padding: '1rem',
		position: 'sticky',
		height: '100vh',
		display: 'flex',
		flexDirection: 'column',
		paddingBottom: '5.5rem',
	},
	subText: {
		textAlign: 'center',
		paddingBottom: '0.5rem',
	},
}))

export default function LeftSideMenu() {
	const classes = useStyles()

	const [tags, setTags] = useState([])
	useEffect(() => {
		TagService.getPopularTags(10).then(res => setTags(res))
	}, [])

	return (
		<Paper square elevation={1} className={classes.root}>
			<Typography variant="h5" className={classes.subText}>
				Menu
			</Typography>
			<Divider />
			<MenuList />

			<Typography variant="h5" className={classes.subText}>
				Tags
			</Typography>
			<Divider />
			{tags && <MenuTags tags={tags} />}

			<MenuFooter />
		</Paper>
	)
}
