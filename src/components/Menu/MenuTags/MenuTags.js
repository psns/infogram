// MUI Imports
import { Box, Chip } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

import { NumberService } from '@/services/number'
import { useHistory } from 'react-router'

const useStyles = makeStyles(theme => ({
	tagsBox: {
		paddingBlock: '1rem',
		display: 'inline-flex',
		flexWrap: 'wrap',
		justifyContent: 'center',
		overflow: 'hidden',
	},
	tag: {
		margin: '0.1rem',
	},
}))

export default function MenuTags({ tags }) {
	const classes = useStyles()
	const history = useHistory()
	/**
	 * Handler for tag redirect
	 *
	 * @param {string} tag
	 */
	function handleTag(tag) {
		history.push(`/tag/${encodeURIComponent(tag)}`)
	}

	return (
		<Box className={classes.tagsBox}>
			{tags.map(({ tag, count }, idx) => {
				return (
					<Chip
						variant="outlined"
						color="secondary"
						key={idx}
						label={`${tag} [${NumberService.format(count, 2)}]`}
						className={classes.tag}
						onClick={() => handleTag(tag)}
					/>
				)
			})}
		</Box>
	)
}
