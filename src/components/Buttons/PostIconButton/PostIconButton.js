import { Button, Tooltip } from '@material-ui/core'

export default function PostIconButton({
	title,
	onClick,
	icon,
	type,
	btnType,
	...props
}) {
	return (
		<Tooltip title={title}>
			<span>
				<Button
					color={(type === btnType && 'secondary') || 'inherit'}
					onClick={onClick}
					{...props}
				>
					{icon}
				</Button>
			</span>
		</Tooltip>
	)
}
