import { Button, CircularProgress, Tooltip } from '@material-ui/core'

export default function LoadingButton({
	loading,
	endIcon,
	children,
	tooltip = 'CONTINUE',
	...props
}) {
	if (loading) {
		return (
			<Button disabled endIcon={<CircularProgress />} {...props}>
				{children}
			</Button>
		)
	} else {
		return (
			<Tooltip title={tooltip}>
				<Button endIcon={endIcon} {...props}>
					{children}
				</Button>
			</Tooltip>
		)
	}
}
