import { useState } from 'react'

import {
	FormControl,
	FormHelperText,
	IconButton,
	Input,
	InputAdornment,
	InputLabel,
	makeStyles,
	TextField,
} from '@material-ui/core'
import { Visibility, VisibilityOff } from '@material-ui/icons'

const useStyles = makeStyles(theme => ({
	formControl: {
		marginTop: '1rem',
	},
	input: {
		color: 'secondary',
	},
}))

export default function PasswordField({
	id,
	label,
	onChange,
	visibleToggleable,
	...props
}) {
	const classes = useStyles()

	const [passwordValue, setPasswordValue] = useState('')
	const [passwordError, setPasswordError] = useState('')
	const [passwordVisible, setPasswordVisible] = useState(false)

	function handleClickShowPassword() {
		setPasswordVisible(!passwordVisible)
	}

	function handleInputChange(event) {
		setPasswordValue(event.target.value)
		// Password contain space
		if (event.target.value.match('[ ]')) {
			setPasswordError(`${label} contain invalid characters.`)
		}
		// Password empty
		else if (event.target.value.match('^$')) {
			setPasswordError(`${label} cannot be empty.`)
		}
		// Password length >= 8
		else if (event.target.value.length < 8) {
			setPasswordError(`${label} length must be greater than 8.`)
		} else {
			setPasswordError('')
		}

		// Callback function
		onChange(event)
	}

	if (visibleToggleable) {
		return (
			<FormControl fullWidth className={classes.formControl}>
				<InputLabel error={Boolean(passwordError)} htmlFor={id}>
					{label} *
				</InputLabel>
				<Input
					id={id}
					type={passwordVisible ? 'text' : 'password'}
					value={passwordValue}
					error={Boolean(passwordError)}
					className={classes.input}
					fullWidth
					onChange={handleInputChange}
					{...props}
					endAdornment={
						<InputAdornment position="end">
							<IconButton
								aria-label={`Toggle ${label} visibility`}
								onClick={handleClickShowPassword}
							>
								{passwordVisible ? <Visibility /> : <VisibilityOff />}
							</IconButton>
						</InputAdornment>
					}
				/>
				<FormHelperText error>{passwordError}</FormHelperText>
			</FormControl>
		)
	} else {
		return (
			<>
				<TextField
					id={id}
					label={label}
					value={passwordValue}
					onChange={handleInputChange}
					error={Boolean(passwordError)}
					className={classes.input}
					required
					fullWidth
					margin="normal"
					type="password"
					{...props}
				/>
				<FormHelperText error>{passwordError}</FormHelperText>
			</>
		)
	}
}
