import { useState } from 'react'

import { FormHelperText, TextField } from '@material-ui/core'

export default function EmailField({ id, label, onChange, ...props }) {
	const [emailValue, setEmailValue] = useState('')
	const [emailError, setEmailError] = useState('')

	// Verify input
	function handleInputChange(event) {
		setEmailValue(event.target.value)
		if (event.target.value.match('^$')) {
			setEmailError('Email cannot be empty.')
		} else if (!event.target.value.match('^\\S+@\\S+$')) {
			setEmailError('Email invalid')
		} else {
			setEmailError('')
		}

		// onChange Callback
		onChange(event)
	}

	return (
		<>
			<TextField
				id={id}
				label={label}
				margin="normal"
				defaultValue={emailValue}
				required
				error={Boolean(emailError)}
				fullWidth
				color="secondary"
				onChange={handleInputChange}
				autoComplete="email"
				autoFocus
				{...props}
			/>
			<FormHelperText error>{emailError}</FormHelperText>
		</>
	)
}
