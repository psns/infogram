import { useState } from 'react'

import { Grid } from '@material-ui/core'
import {
	KeyboardDatePicker,
	MuiPickersUtilsProvider,
} from '@material-ui/pickers'
import LuxonUtils from '@date-io/luxon'

export default function DateField({ id, label, onChange, ...props }) {
	const [dateInput, setDateInput] = useState(null)

	function handleDateInputChange(date) {
		setDateInput(date)
		onChange(date)
	}

	return (
		<>
			<MuiPickersUtilsProvider utils={LuxonUtils}>
				<Grid container justify="space-around">
					<KeyboardDatePicker
						id={id}
						label={`${label} *`}
						value={dateInput}
						margin="normal"
						fullWidth
						maxDate={new Date()}
						format="dd/MM/yyyy"
						color="secondary"
						onChange={handleDateInputChange}
						KeyboardButtonProps={{
							'aria-label': `Change ${label} date`,
						}}
						{...props}
					/>
				</Grid>
			</MuiPickersUtilsProvider>
		</>
	)
}
