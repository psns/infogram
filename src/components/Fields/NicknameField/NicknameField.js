import { useState } from 'react'

import { FormHelperText, InputAdornment, TextField } from '@material-ui/core'

export default function NicknameField({ id, onChange, ...props }) {
	const [nicknameInput, setNicknameInputValue] = useState('')
	const [nicknameError, setNicknameError] = useState('')

	// Verify input
	function handleNicknameInputChange(event) {
		setNicknameInputValue(event.target.value)
		if (event.target.value.match('^$')) {
			setNicknameError('Nickname cannot be empty.')
		} else if (!event.target.value.match('^[a-zA-Z0-9_]+$')) {
			setNicknameError('Nickname contain invalid charaters.')
		} else if (event.target.value.length > 15) {
			setNicknameError('Nickname length must be lower than 15 characters')
		} else {
			setNicknameError('')
		}

		// onChange Callback
		onChange(event)
	}

	return (
		<>
			<TextField
				id={id}
				margin="normal"
				required
				error={Boolean(nicknameError)}
				fullWidth
				color="secondary"
				label="Nickname"
				defaultValue={nicknameInput}
				onChange={handleNicknameInputChange}
				InputProps={{
					startAdornment: <InputAdornment position="start">@</InputAdornment>,
				}}
				{...props}
			/>
			<FormHelperText error>{nicknameError}</FormHelperText>
		</>
	)
}
