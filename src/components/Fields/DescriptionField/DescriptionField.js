// MUI Imports
import { Grid, makeStyles } from '@material-ui/core'
import {
	Equalizer,
	Event,
	Image,
	InsertEmoticon,
	TextFields,
} from '@material-ui/icons'

// Infogram components
import PostIconButton from '@/components/Buttons/PostIconButton/PostIconButton'
import TextAreaField from '@/components/Fields/TextAreaField/TextAreaField'

// Custom css style
const useStyles = makeStyles(theme => ({
	textarea: {
		width: '100%',
	},
	center: {
		textAlign: 'center',
	},
	inputFile: {
		margin: '1rem 0rem 1rem 0rem',
	},
	grid: {
		margin: '0.2rem',
	},
}))

export default function DescriptionField({
	type,
	description,
	data,
	onDescChange,
	onFileSelect,
	onTypeChange,
}) {
	const classes = useStyles()

	function renderExtra(type) {
		switch (type) {
			case 'MEDIA':
				return (
					// Print data (media if MEDIA type is selected)
					<input
						type="file"
						onChange={event => onFileSelect(event.target.files[0])}
					/>
				)

			case 'SURVEY':
				return <>SURVEY</>

			case 'EVENT':
				return <>EVENT</>

			case 'EMOJI':
				return <>EMOJI</>

			default:
				return <></>
		}
	}

	return (
		<>
			<TextAreaField
				id="post-description"
				label="Description"
				placeholder="Write your #Text here"
				value={description}
				onChange={onDescChange}
			/>

			{renderExtra(type)}

			<Grid
				container
				className={classes.grid}
				direction="row"
				justify="space-between"
				alignItems="flex-start"
			>
				<Grid item sm className={classes.center}>
					<PostIconButton
						title="Simple text"
						type={type}
						btnType="TEXT"
						onClick={() => onTypeChange('')}
						icon={<TextFields />}
					/>
				</Grid>
				<Grid item sm className={classes.center}>
					<PostIconButton
						title="Attach an image"
						type={type}
						btnType="MEDIA"
						onClick={() => onTypeChange('MEDIA')}
						icon={<Image />}
					/>
				</Grid>
				<Grid item sm className={classes.center}>
					<PostIconButton
						title="WIP: Create a survey"
						type={type}
						btnType="SURVEY"
						onClick={() => onTypeChange('SURVEY')}
						icon={<Equalizer />}
					/>
				</Grid>
				<Grid item sm className={classes.center}>
					<PostIconButton
						title="WIP: Schedule an event"
						type={type}
						btnType="EVENT"
						onClick={() => onTypeChange('EVENT')}
						icon={<Event />}
					/>
				</Grid>
				<Grid item sm className={classes.center}>
					<PostIconButton
						title="WIP: Attach emoji"
						type={type}
						btnType="EMOJI"
						onClick={() => onTypeChange('EMOJI')}
						icon={<InsertEmoticon />}
					/>
				</Grid>
			</Grid>

			{}
		</>
	)
}
