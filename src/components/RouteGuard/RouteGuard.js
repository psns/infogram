import React from 'react'
import { Redirect, Route } from 'react-router'

// Infogram services
import { UserService } from '@services/user'

/**
 * Protected route. User as to be authenticated to see the component
 *
 * @param {Object} Object React props (must contain path, component and redirectTo attributes)
 * @returns JSX Elements
 */
export default function RouteGuard({ path, component, redirectTo, ...rest }) {
	if (UserService.getJwtToken()) {
		return <Route path={path} component={component} {...rest} />
	}
	return <Redirect to={redirectTo} />
}
