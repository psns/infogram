import {
	Card,
	CardContent,
	Container,
	makeStyles,
	Typography,
} from '@material-ui/core'

import Loading from '@components/Loading/Loading'

// Custom css style
const useStyles = makeStyles(theme => ({
	image: {
		margin: theme.spacing(1),
		width: '100px',
		height: '100px',
	},
	cardContent: {
		padding: theme.spacing(5),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	backdrop: {
		zIndex: theme.zIndex.drawer + 1,
		color: '#fff',
	},
	title: {
		paddingBottom: '15px',
	},
}))

export default function BlankLayout({ loading, className, children }) {
	const classes = useStyles()

	return (
		<Container className={className}>
			{/* Content */}
			<Card>
				<CardContent className={classes.cardContent}>
					<img
						src="/infogram.png"
						alt="Infogram logo"
						className={classes.image}
					/>
					<Typography variant="h4" noWrap className={classes.title}>
						&#60;Infogram &#47;&#62;
					</Typography>

					{children}
				</CardContent>
			</Card>

			{/* Loading component */}
			<Loading active={loading} />
		</Container>
	)
}
