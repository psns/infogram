import { useEffect, useState } from 'react'
import { useLocation } from 'react-router'

// MUI Imports
import { Box, Grid, makeStyles } from '@material-ui/core'

// Infogram components
import Loading from '@components/Loading/Loading'
import Navbar from '@components/Navbar/Navbar'
import PostPanel from '@/components/Panels/Post'
import LeftSideMenu from '@/components/Menu/LeftSideMenu'
import NotificationsPanel from '@/components/Panels/Notification'
import MessagesPanel from '@/components/Panels/Messages'

// Custom css style
const useStyles = makeStyles(theme => ({
	content: {
		paddingTop: '4rem',
		height: 'inherit',
	},
	middle: {
		height: 'inherit',
	},
}))

export default function MainLayout({ loading, children }) {
	const classes = useStyles()

	// get query params
	const search = new URLSearchParams(useLocation().search)
	const parentId = search.get('new-comment')

	const [isPostPanelDisplay, setPostPanelDisplay] = useState(false)
	const [isNotifPanelDisplay, setNotifPanelDisplay] = useState(false)
	const [isMessagePanelDisplay, setMessagePanelDisplay] = useState(false)

	useEffect(() => {
		setPostPanelDisplay(Boolean(parentId))
		setNotifPanelDisplay(false)
		setMessagePanelDisplay(false)
	}, [parentId])

	function handleNavPostClick() {
		setPostPanelDisplay(!isPostPanelDisplay)
		setNotifPanelDisplay(false)
		setMessagePanelDisplay(false)
	}

	function handleCloseRightPanel() {
		setPostPanelDisplay(false)
		setNotifPanelDisplay(false)
		setMessagePanelDisplay(false)
	}

	function handleNavNotifClick() {
		setPostPanelDisplay(false)
		setNotifPanelDisplay(true)
		setMessagePanelDisplay(false)
	}

	function handleNavMessageClick() {
		setPostPanelDisplay(false)
		setNotifPanelDisplay(false)
		setMessagePanelDisplay(true)
	}

	return (
		<>
			{/* Navbar component */}
			<Navbar
				onPostClick={handleNavPostClick}
				onNotifClick={handleNavNotifClick}
				onMessageClick={handleNavMessageClick}
			/>

			{/* Content */}
			<Grid
				container
				direction="row"
				justify="space-evenly"
				alignItems="stretch"
				className={classes.content}
			>
				{/* Left side menu component */}
				<Grid item sm={2}>
					<LeftSideMenu />
				</Grid>

				{/* Main content */}
				<Grid item sm={7} className={classes.middle}>
					<Box mx={1} className={classes.middle}>
						{children}
					</Box>
				</Grid>

				{/* Rigth side menu component */}
				<Grid item sm={3}>
					{isPostPanelDisplay && (
						<PostPanel parentId={parentId} onReturn={handleCloseRightPanel} />
					)}
					{isNotifPanelDisplay && <NotificationsPanel />}
					{isMessagePanelDisplay && <MessagesPanel />}
				</Grid>
			</Grid>

			{/* Loading component */}
			<Loading active={loading} />
		</>
	)
}
