import { createContext, useState } from 'react'

const AppContext = createContext()

/**
 * App custom Provider
 */
export function AppProvider({ children }) {
	const [value, setValue] = useState({
		user: null,
	})

	return (
		<AppContext.Provider
			value={{
				value: value,
				setValue: setValue,
			}}
		>
			{children}
		</AppContext.Provider>
	)
}

export const AppConsumer = AppContext.Consumer
export default AppContext
