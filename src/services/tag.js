import axios from 'axios'

/**
 * Create tags and link them with a post
 *
 * @param {string} postId
 * @param {Array<Object>} tagList array of tag -> { name: <name> }
 *
 * @returns Axios result
 */
function newTags(postId, tagList = []) {
	return axios
		.put(`/api/tag/post/${postId}`, {
			tags: tagList,
			withCredentials: true,
		})
		.then(res => res.data)
		.catch(err => err)
}

/**
 * Get popular tags
 *
 * @param {number} number
 * @returns Post[]
 */
function getPopularTags(number) {
	return axios
		.get(`/api/tag/count/post`)
		.then(res => {
			let tagLength = res.data.length
			let tags = res.data.splice(
				0,
				number <= tagLength - 1 ? number : tagLength - 1
			)
			return tags
		})
		.catch(err => err)
}

/**
 * Get post with specific tag
 *
 * @param {number} page
 * @param {string} tag
 * @returns Post[]
 */
function getPostTag(page, tag, until) {
	return axios
		.post(`/api/post/tag/${tag}?page=${page}`, { until: until })
		.then(res => res.data)
		.catch(err => err)
}

export const TagService = {
	newTags,
	getPopularTags,
	getPostTag,
}
