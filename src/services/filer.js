import axios from 'axios'

/**
 * Upload file in filer
 *
 * @param {file} file file to upload
 * @param {string} path userId
 * @param {string} context Context of file (background, profile, postId or groupId)
 */
function upload(file, path, context) {
	let formData = new FormData()
	formData.append('file', file)
	formData.append('path', path)
	formData.append('context', context)

	return axios
		.post(`/api/filer/upload`, formData, {
			headers: {
				'Content-Type': 'multipart/form-data',
			},
		})
		.then(res => res.data)
		.catch(err => err)
}

export const FilerService = {
	upload,
}
