import axios from 'axios'

// Infogram services
import { UserService } from './user'
// eslint-disable-next-line
import { TagService } from './tag'
// eslint-disable-next-line
import { FilerService } from './filer'

function newPost(post) {
	const userId = UserService.getUserId()
	// eslint-disable-next-line
	const { type, tags, visibility, description, data, groupId, parentId } = post

	return axios
		.post(`/api/post/${userId}`, {
			type: type,
			description: description,
			content: data,
			visibility: visibility,
			parentId: parentId,
			groupId: groupId,
			withCredentials: true,
		})
		.then(res => {
			// Upload file if post type is MEDIA
			if (type === 'MEDIA') {
				FilerService.upload(data, userId, res.data.id)
					.then(res => console.debug(res))
					.catch(err => console.error(err))
			}

			// Create tags
			let tagsObject = []
			tags.forEach(tag => {
				tagsObject.push({ name: tag })
			})
			TagService.newTags(res.data.id, tagsObject).then(res =>
				console.debug(res)
			)
		})
		.catch(err => {
			console.error(err)
		})
}

/**
 * Get follower posts
 *
 * @param {string} userId
 * @param {number} page
 * @returns Post[]
 */
function getFollowerPost(userId, page) {
	return axios
		.get(`/api/post/follower/${userId}?page=${page}`, {
			withCredentials: true,
		})
		.then(res => res.data)
		.catch(err => err)
}

/**
 * Get response post
 *
 * @param {string} page
 * @param {number} date
 * @returns Post
 */
function getPopularPost(page, until) {
	return axios
		.post(
			`/api/post/popular?page=${page}`,
			{
				until: until,
			},
			{
				withCredentials: true,
			}
		)
		.then(res => res.data)
		.catch(err => err)
}

/**
 * Get response post
 *
 * @param {string} postId
 * @returns Post
 */
function getResponsePost(postId, page) {
	return axios
		.get(`/api/post/response/${postId}?page=${page}`, {
			withCredentials: true,
		})
		.then(res => res.data)
		.catch(err => err)
}

/**
 * Get user post
 *
 * @param {string} userId
 * @returns Post
 */
function getUserPost(userId, page) {
	return axios
		.get(`/api/post/${userId}?page=${page}`, {
			withCredentials: true,
		})
		.then(res => res.data)
		.catch(err => err)
}

/**
 * Get post by its id
 *
 * @param {string} postId
 * @returns Post
 */
function getPost(postId) {
	return axios
		.get(`/api/post/id/${postId}`, { withCredentials: true })
		.then(res => res.data)
		.catch(err => err.response.data)
}

/**
 * Get nb response of post
 *
 * @param {string} postId
 * @returns number
 */
function getNbResponse(postId) {
	return axios
		.get(`/api/post/response/nb/${postId}`, { withCredentials: true })
		.then(res => res.data)
		.catch(err => err)
}

/**
 * Delete a post
 *
 * @param {string} postId
 * @returns Post
 */
function deletePost(postId) {
	return axios
		.delete(`/api/post/${postId}`, { withCredentials: true })
		.then(res => res.data)
		.catch(err => err.response.data)
}

/**
 * Get badge post
 *
 * @param {string} postId
 * @returns Badge[]
 */
function getBadgePost(postId) {
	return axios
		.get(`/api/post/badge/${postId}`, { withCredentials: true })
		.then(res => res.data)
		.catch(err => err)
}

/**
 * Get all badges
 *
 * @returns Badge[]
 */
function getBadges() {
	return axios
		.get('/api/post/badge', { withCredentials: true })
		.then(res => res.data)
		.catch(err => err)
}

/**
 * Get score of post
 *
 * @param {string} postId
 * @returns number
 */
function getScore(postId) {
	return axios
		.get(`/api/post/score/${postId}`, { withCredentials: true })
		.then(res => res.data)
		.catch(err => err)
}

/**
 * Get user score for a post
 *
 * @param {string} userId
 * @param {string} postId
 * @returns Score
 */
function getUserScore(userId, postId) {
	return axios
		.get(`/api/post/score/${userId}/${postId}`, { withCredentials: true })
		.then(res => res.data)
		.catch(err => err)
}

/**
 * Assign score post
 *
 * @param {string} userId
 * @param {string} postId
 * @param {number} value
 */
function assignScore(userId, postId, value) {
	return axios
		.post(`/api/post/score/${userId}`, {
			postId: postId,
			value: value,
			withCredentials: true,
		})
		.then(res => res.data)
		.catch(err => err)
}

export const PostService = {
	newPost,
	deletePost,

	getFollowerPost,
	getPost,
	getPopularPost,
	getResponsePost,
	getUserPost,
	getNbResponse,
	getBadges,
	getBadgePost,
	getScore,
	getUserScore,

	assignScore,
}
