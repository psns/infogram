const { PrismaClient, PostType, Visibility, Role } = require('@prisma/client')

const argon2id = require('argon2')

const prisma = new PrismaClient()

// Pre-defined Tags
const tagList = [
	{ name: 'Funny' },
	{ name: 'ProgramingHorror' },
	{ name: 'Entertainment' },
	{ name: 'Learning' },
	{ name: 'News' },
	{ name: 'Environment' },
	{ name: 'Java' },
	{ name: 'JavaScript' },
	{ name: 'C/C++' },
	{ name: 'Golang' },
	{ name: 'RFC' },
]

const badgesList = [
	{ name: 'ad', icon: 'badge/ad.png' },
	{ name: 'birth', icon: 'badge/birthday-cake.png' },
	{ name: 'learn', icon: 'badge/book.png' },
	{ name: 'crypto', icon: 'badge/crypto.png' },
	{ name: 'money', icon: 'badge/money.png' },
	{ name: 'news', icon: 'badge/newspaper.png' },
]

async function main() {
	await prisma.score.deleteMany()
	await prisma.badgePost.deleteMany()
	await prisma.badge.deleteMany()
	await prisma.favorite.deleteMany()
	await prisma.tag.deleteMany()
	await prisma.groupMember.deleteMany()
	await prisma.group.deleteMany()
	await prisma.badge.deleteMany()
	await prisma.post.deleteMany()
	await prisma.credential.deleteMany()
	await prisma.followers.deleteMany()
	await prisma.profile.deleteMany()
	await prisma.message.deleteMany()
	await prisma.user.deleteMany()

	// Create 10 Users
	await createUsers(10)

	// Create tags
	await createTags()

	// Create between 2 and 10 posts per user
	await createPost(2, 10)

	// Create between 0 and 10 followers
	await createFollowers()

	await createBadge()

	await createAdmin()
}

/** Create Users
 *
 * @param {Integer} number Number of user to create
 */
async function createUsers(number) {
	for (let idx = 0; idx < number; idx++) {
		await prisma.user.create({
			data: {
				name: `test_${idx}`,
				nickname: `test_${idx}`,
				email: `test.test.${idx}@test.io`,
				Credential: {
					create: {
						hash: await argon2id.hash(`test_${idx}`),
					},
				},
			},
		})

		// Create associated profile
		await createProfiles(idx)
	}
}

/** Create admin
 */
async function createAdmin() {
	await prisma.user.create({
		data: {
			name: 'admin',
			nickname: 'admin',
			email: 'admin@admin.com',
			role: Role.ADMIN,
			Credential: {
				create: {
					hash: await argon2id.hash('admin'),
				},
			},
			Profile: {
				create: {
					description: "I'm god in this app and I love Quenouille !",
					visibility: Visibility.PUBLIC,
				},
			},
		},
	})
	await prisma.user.create({
		data: {
			name: 'Dev Dev',
			nickname: 'dev',
			email: 'dev@dev.com',
			role: Role.ADMIN,
			Credential: {
				create: {
					hash: await argon2id.hash('dev'),
				},
			},
			Profile: {
				create: {
					description:
						'My name is Dev Dev and I dev. I dev the best dev from entire dev. #dev',
					visibility: Visibility.PUBLIC,
				},
			},
		},
	})
}

/** Create profile associated with a user
 *
 * @param {Integer} userIdx Index of user
 */
async function createProfiles(userIdx) {
	await prisma.profile.create({
		data: {
			User: {
				connect: {
					email: `test.test.${userIdx}@test.io`,
				},
			},
			description: `Youu houuuuuuu ! My nickname is test_${userIdx} ! I'm so happy :(`,
			visibility: Visibility.PRIVATE,
		},
	})
}

/** Create a certain number of post per user on random group
 *
 * @param {Integer} minNbPost Minimum number of post per user
 * @param {Integer} maxNbPost Maximum number of post per user
 */
async function createPost(minNbPost, maxNbPost) {
	for (let userIdx = 0; userIdx < 10; userIdx++) {
		let postNumber = Math.floor(Math.random() * maxNbPost - 1) + minNbPost

		for (let postIdx = 0; postIdx < postNumber; postIdx++) {
			let post = await prisma.post.create({
				data: {
					description: "J'ai vu des Quenouilles !!!!",
					Publisher: {
						connect: {
							email: `test.test.${userIdx}@test.io`,
						},
					},
					type: PostType.TEXT,
				},
			})

			// Create sub posts
			await createSubPost(post.id)
		}
	}
}

/** Create subpost associated with a parentpost and a group
 *
 * @param {Integer} parentId Post Parent ID
 */
async function createSubPost(parentId) {
	for (let idx = 0; idx < 3; idx++) {
		let nbUser = await prisma.user.count()
		let rdmUserId = Math.floor(Math.random() * nbUser)

		await prisma.post.create({
			data: {
				description: "J'aime troooop, humm !",
				type: PostType.TEXT,
				Parent: {
					connect: {
						id: parentId,
					},
				},
				Publisher: {
					connect: {
						email: `test.test.${rdmUserId}@test.io`,
					},
				},
			},
		})
	}
}

/** Generate tags
 */
async function createTags() {
	await prisma.tag.createMany({
		data: tagList,
		skipDuplicates: true,
	})
}

/** Generate badge
 */
async function createBadge() {
	await prisma.badge.createMany({
		data: badgesList,
	})
}

/** Create followers
 *
 * @param {Integer} number Number of followers to create
 */
async function createFollowers() {
	let nbUsers = await prisma.user.count()
	for (let idxUser = 0; idxUser < nbUsers; idxUser++) {
		for (let idx = 0; idx < Math.floor(Math.random() * 10); idx++) {
			await prisma.followers.create({
				data: {
					Followed: {
						connect: {
							email: `test.test.${idxUser}@test.io`,
						},
					},
					Follower: {
						create: {
							email: `test.follower.${idxUser}-${idx}@test.io`,
							nickname: `test_follower_${idxUser}-${idx}`,
							name: `TEST FOLLOWERS ${idxUser}-${idx}`,
							Credential: {
								create: {
									hash: await argon2id.hash(`test_follower_${idx}`),
								},
							},
						},
					},
				},
			})
		}
	}
}

main()
	.catch(e => {
		throw e
	})
	.finally(async () => {
		await prisma.$disconnect()
	})
